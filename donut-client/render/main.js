/*
 * @Date: 2022-07-10 10:00
 */
const electron = require('electron');
const kill = require("tree-kill");
const { ipcMain, app, BrowserWindow, globalShortcut,dialog} = electron
// 支持原生模块
app.allowRendererProcessReuse = false;
const remote = require("@electron/remote/main");

const dev = false//是否为开发环境：打包专用
// const dev = true//测试

// 日志
const logger = require('electron-log');
// logger.transports.file.level = 'info'
logger.transports.file.level = 'error'
logger.transports.file.maxSize = 1002430 // 最大不超过10M
logger.transports.file.format = '[{y}-{m}-{d} {h}:{i}:{s}.{ms}] [{level}]{scope} {text}' // 设置文件内容格式
logger.transports.file.fileName = 'donut.log' // 创建文件名为 donut.log
// 指定日志文件夹位置
const dataPath = dev?'D:/Go/src/practice/playv3/donut-client/userData/':'F:/userData/'
logger.transports.file.resolvePathFn = ()=> dataPath+'donut.log'

var mainWindow = null // 主窗口
// 当app准本就绪之后创建窗体
app.on('ready', createWindow)
// app代表electron的引用，BrowserWindow代表窗体
function createWindow () {
    //运行后端程序
    startServer()
    // 创建窗体
    mainWindow = new BrowserWindow({
        // 距屏幕左上角位置
        x: 470,
        y: 717,
        // 初始大小
        width: 400,
        height: 164,
        webPreferences: {
            // 为了使用node
            nodeIntegration: true,
            contextIsolation: false,
            plugins: true,
        },
        // 无边框
        frame:false,
        //透明
        transparent:true,
        //隐藏任务栏
        skipTaskbar:true,
        // 禁止最大化
        maximizable: false,
        // 禁止全屏
        fullscreen: false,
    })
    //引入remote
    remote.initialize()
    remote.enable(mainWindow.webContents)
    //设置前端端口
    const urlLocation ='http://localhost:8708'//cra默认开的3000端口
    mainWindow.loadURL(urlLocation)
    // 加载渲染进程文件
    if (dev){
        //测试环境
        mainWindow.loadFile('index.html')
    }else{
        //生产环境
        mainWindow.loadFile('render/index.html')
    }
    // 窗体唤醒快捷键
    globalShortcut.register('CommandOrControl+Shift+Space', function () {
        // 已关闭，则开启新的
        if (mainWindow == null) {
            createWindow()
        }
        // 没关闭，就恢复原来的
        if (mainWindow != null){
            mainWindow.restore();
        }
    })
    //获得焦点
    mainWindow.on('focus', () => {
        //全局搜索
        globalShortcut.register('CommandOrControl+F', function () {
            if (mainWindow && mainWindow.webContents) {
                mainWindow.webContents.send('on-find', '')
            }
        })
        // 字号放大缩小
        globalShortcut.register('CommandOrControl+=', function () {
            if (mainWindow && mainWindow.webContents) {
                mainWindow.webContents.send('big', '')
            }
        })
        globalShortcut.register('CommandOrControl+-', function () {
            if (mainWindow && mainWindow.webContents) {
                mainWindow.webContents.send('sm', '')
            }
        })
        //禁用全局放大
        globalShortcut.register('CommandOrControl+Shift+=', function () {
        })
        // 退出程序快捷键
        globalShortcut.register('CommandOrControl+D', function () {
            mainWindow.webContents.send('save', '')//停止翻页&保存浏览记录
            stopServer()//停止后端程序
            app.quit()//退出app
        })
        // 刷新
        globalShortcut.register('CommandOrControl+R', function () {
            mainWindow.webContents.send('refresh', '')
        })
        // 不保存阅读记录，返回书架
        globalShortcut.register('CommandOrControl+Q', function () {
            mainWindow.webContents.send('quit', '')
        })
        // 翻页
        globalShortcut.register('CommandOrControl+Space', function () {
            if (mainWindow && mainWindow.webContents) {
                mainWindow.webContents.send('scroll', '')
            }
        })
        // 回到顶部
        globalShortcut.register('CommandOrControl+T', function () {
            mainWindow.webContents.send('to-top', '')
        })
        // 写作回到底部
        globalShortcut.register('CommandOrControl+E', function () {
            mainWindow.webContents.send('to-end', '')
        })
        // 记事本
        globalShortcut.register('CommandOrControl+P', function () {
            mainWindow.webContents.send('save', '')//停止翻页&保存浏览记录（获取文件名）
            mainWindow.webContents.send('share-points', '')
        })
        // 下一章
        globalShortcut.register('CommandOrControl+N', function () {
            mainWindow.webContents.send('next-chapter', '')
        })
        // 上一章
        globalShortcut.register('CommandOrControl+Shift+N', function () {
            mainWindow.webContents.send('last-chapter', '')
        })
        // 收藏帖子
        globalShortcut.register('CommandOrControl+L', function () {
            mainWindow.webContents.send('like-topic', '')
        })
        // 显示阅读进度
        globalShortcut.register('CommandOrControl+U', function () {
            mainWindow.webContents.send('read-schedule', '')
        })
        // 添加弹幕
        globalShortcut.register('CommandOrControl+M', function () {
            mainWindow.webContents.send('add-danmu', '')
        })
    })
    //失去焦点
    mainWindow.on('blur', () => {
        //停止翻页&保存浏览记录
        mainWindow.webContents.send('save', '')
        // 防止尴尬最小化
        mainWindow.minimize()
        // 注销全局快捷键
        globalShortcut.unregister('CommandOrControl+F')
        globalShortcut.unregister('CommandOrControl+=')
        globalShortcut.unregister('CommandOrControl+-')
        globalShortcut.unregister('CommandOrControl+D')
        globalShortcut.unregister('CommandOrControl+R')
        globalShortcut.unregister('CommandOrControl+Q')
        globalShortcut.unregister('CommandOrControl+Space')
        globalShortcut.unregister('CommandOrControl+Shift+=')
        globalShortcut.unregister('CommandOrControl+T')
        globalShortcut.unregister('CommandOrControl+E')
        globalShortcut.unregister('CommandOrControl+P')
        globalShortcut.unregister('CommandOrControl+Shift+N')
        globalShortcut.unregister('CommandOrControl+N')
        globalShortcut.unregister('CommandOrControl+L')
        globalShortcut.unregister('CommandOrControl+U')
        globalShortcut.unregister('CommandOrControl+M')
    })
    // 禁用全局缩放
    mainWindow.webContents.on("did-finish-load", (event, args) => {
        mainWindow.webContents.setZoomFactor(1);
        mainWindow.webContents.setVisualZoomLevelLimits(1, 1);
    })
}

app.on('activate', () => {
    if (mainWindow === null) createWindow()
    // 没关闭，就恢复原来的
    if (mainWindow != null){
        mainWindow.restore();
    }
})

// 开启后端程序
var serverProcess=null;
function startServer(){
    // 启动后台服务的命令
    let cmdStr = "donut-server.exe"
    // 测试环境server路径 生产环境server路径
    let cmdPath = dev?"./server":".\\resources\\app\\server"
    runExec(cmdStr,cmdPath)
    function runExec(cmdStr,cmdPath){
        // 在启动后台服务前，检测关闭一遍后台服务，防止开启多个后台服务
        stopServer();
        serverProcess = require('child_process').spawn(cmdStr,{cwd:cmdPath})
        // 启动成功的输出
        serverProcess.stdout.on("data",function(data){
            // logger.info("启动donut-server服务器成功！ stdout:" + data)
        })
        // 发生错误的输出
        serverProcess.stderr.on("data",function(data){
            logger.error("启动donut-server发生错误：stderr:" + data)
        })
        // 退出后的输出
        serverProcess.on("close",function(code){
            // logger.info("已关闭donut-server！out code:" + code)
        })
    }
}
// 关闭后台服务
function stopServer(){
    if(serverProcess){
        kill(serverProcess.pid,"SIGTERM",function(){
            serverProcess = null;
        })
    }
}

// 请求单一实例锁，返回false表示已经有一个实例在运行
const gotTheLock = app.requestSingleInstanceLock();
if (!gotTheLock) {
    // 已经有一个实例在运行，不做任何等待（https://www.cnblogs.com/imgss/p/13290418.html），彻底关闭当前实例
    app.exit();
} else {
    // 监听第二个实例被运行时
    app.on("second-instance", () => {
        // 当有第二个实例被运行时，激活之前的实例并将焦点置于其窗口
        if (mainWindow){
            // 最小化就恢复
            if(mainWindow.isMinimized()){
                mainWindow.restore();
            }
            // 激活主窗口
            mainWindow.focus();
        }
    });
}

ipcMain.on("select-file",(event, arg) => {
    // 打开文件对话框
    dialog.showOpenDialog({
        title: '选择文件',
        buttonLabel: '选择文件',
        properties: ['openFile']
    }).then(result => {
        if (!result.canceled) {
            // 用户选择的文件路径
            event.reply("file-path", result.filePaths[0]);
        }
        mainWindow.restore();
    }).catch(err => {
        mainWindow.restore();
    });
})

ipcMain.on("save-file",(event,arg)=>{
    dialog.showSaveDialog({
        title: '保存文件',
        buttonLabel: '保存',
        defaultPath:arg
    }).then(result => {
        if (!result.canceled) {
            // 用户选择的文件路径
            event.reply("save-dialog", result.filePath);
        }
        mainWindow.restore();
    }).catch(err => {
        mainWindow.restore();
    });

})

// 日志
ipcMain.handle('logger', (event, level,arg)=> {
    if (level === "info") {
        logger.info(arg);
    } else if (level === 'warn') {
        logger.warn(arg);
    } else if (level === 'error') {
        logger.error(arg);
    } else if (level === 'debug') {
        logger.debug(arg);
    }
})

// //禁止加载图片/视频
// session.defaultSession.webRequest.onBeforeRequest((details, callback) => {
//     if (details.url.endsWith('.jpg') || details.url.endsWith('.jpeg') || details.url.endsWith('.png') || details.url.endsWith('.gif') || details.url.endsWith('.svg') || details.url.endsWith('.webp') || details.url.endsWith('.mp4') || details.url.endsWith('.webm')) {
//         callback({ cancel: true });
//     } else {
//         callback({ cancel: false });
//     }
// });

// // 百度搜索
// ipcMain.on('baidu-search', (event, searchQuery) => {
//     const url = `https://www.baidu.com/s?wd=${encodeURIComponent(searchQuery)}`;
//     mainWindow.loadURL(url);
// });