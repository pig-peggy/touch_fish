const { ipcRenderer} = require('electron');
const { FindInPage } = require('../src');
const fs = require("fs");
const path = require("path");
const remote = require('@electron/remote');
const Menu = remote.Menu;
const MenuItem = remote.MenuItem;

// 文件名
var FileName = "";

const dev = false//是否为开发环境：打包专用
// const dev = true//测试

// 用户数据的根目录
const dataPath = dev?'D:/Go/src/practice/playv3/donut-client/userData/':'F:/userData/'
// 浏览记录
const readPath = path.join(dataPath,'readingHistory.json');
// 文字色卡路径
const colorPath = path.join(dataPath,'colorConfig.json');
// 背景色卡路径
const bgPath = path.join(dataPath,'backgroundConfig.json');
// 点评路径
const pointPath = path.join(dataPath,'readingPoint.json');
// 目录正则路径
const dirPath = path.join(dataPath,'dirRules.json');
// 书架路径
const shelfPath = path.join(dataPath,'bookShelf.json');
// 网络源规则路径
const searchPath =path.join(dataPath,"searchRules.json");
// 本地小说整理/导出的根目录
const bookPath = dev?'D:/Go/src/practice/playv3/donut-client/userData/testReader/':'F:/reader/'
// 用户设置路径
const configPath = path.join(dataPath,'userConfig.json');
// 豆瓣小组收藏夹路径
const groupPath = path.join(dataPath,'douGroup.json');
// 豆瓣帖子收藏夹路径
const topicPath = path.join(dataPath,'douTopic.json');
// 豆瓣记事本
const douPointPath = path.join(dataPath,'dou.txt');
// 默认写作保存路径
const writerPath = path.join(dataPath,'writer.txt');
// 写作记事本
const writerPointPath = path.join(dataPath,'writerPoint.txt');
// 备份路径
let now = new Date();
let year = now.getFullYear(); // 年
let month = now.getMonth() + 1; // 月
let date = now.getDate(); // 日
const backupPath = path.join(dataPath+'backups/'+`${year}-${month}-${date}`+'.txt');

// 主菜单
var mainMenu = new Menu();
var menuColor
var menuBg
initColorMenu()
var menuScroll =new MenuItem({
    label:"翻页",
    submenu:[
        {
            label: "加速",
            click:function (){
                if (scrollSpeed>5){
                    clearInterval(ref);
                    ref=null
                    scrollSpeed=scrollSpeed-5
                    let initHeight = document.documentElement.scrollTop;
                    ref = setInterval(()=>{
                        window.scroll(0 ,++initHeight)
                    },scrollSpeed);
                }
            }
        },
        {
            label: "减速",
            click:function (){
                if (scrollSpeed<200) {
                    clearInterval(ref);
                    ref = null
                    scrollSpeed = scrollSpeed + 5
                    let initHeight = document.documentElement.scrollTop;
                    ref = setInterval(()=>{
                        window.scroll(0 ,++initHeight)
                    },scrollSpeed);
                }
            }
        }
    ]
})
var menuQuickStart=new MenuItem({
    label:"快捷键一览",
    submenu:[
        {
            label:"刷新",
            accelerator: 'Ctrl+R',
        },
        {
            label:"放大字号",
            accelerator: 'Ctrl+=',
        },
        {
            label:'缩小字号',
            accelerator: 'Ctrl+-',
        },
        {
            label:"上一章",
            accelerator: 'Ctrl+N',
        },
        {
            label:"下一章",
            accelerator: 'Ctrl+Shift+N',
        },
        {
            label:"不保存阅读进度",
            accelerator: 'Ctrl+Q',
        },
        {
            label:"显示阅读进度",
            accelerator: 'Ctrl+U',
        },
        {
            label:"搜索",
            accelerator: 'Ctrl+F',
        },
        {
            label:"观后感",
            accelerator: 'Ctrl+P',
        },
        {
            label:"收藏帖子",
            accelerator: 'Ctrl+L',
        },
        {
            label:"回到顶部",
            accelerator: 'Ctrl+T',
        },
        {
            label:"回到底部",
            accelerator: 'Ctrl+E',
        },
        {
            label:"关闭程序",
            accelerator: 'Ctrl+D',
        },
        {
            label: '自动翻页',
            accelerator: 'Ctrl+Space',
        },
        {
            label:"唤醒后台程序",
            accelerator: 'Ctrl+Shift+Space',
        },
        {
            label:"发布弹幕",
            accelerator: 'Ctrl+M',
        }
    ]
})
// 添加到菜单组
mainMenu.append(menuColor)
mainMenu.append(menuBg)
mainMenu.append(menuQuickStart)
function makeColor(fontColor,colorMenu){
    //读取色卡
    let colorData
    if (fontColor){
        colorData = JSON.parse(fs.readFileSync(colorPath, 'utf8'));
    }else {
        colorData = JSON.parse(fs.readFileSync(bgPath, 'utf8'));
    }
    for (let i = 0; i < colorData.length; i++) {
        // 添加到子菜单
        colorMenu.append(new MenuItem({
            label: colorData[i].colorName,
            click: function () {
                if (fontColor){
                    document.body.style.color = colorData[i].codeHEX;
                    let elements = document.getElementsByClassName('color-input');
                    for (let j = 0; j < elements.length; j++) {
                        elements[j].style.color = colorData[i].codeHEX;
                    }
                }else {
                    document.body.style.backgroundColor = colorData[i].codeHEX;
                }
            }
        }))
    }
    colorMenu.append(new MenuItem({
        label: "更多",
        click: function () {
            // 显示弹窗
            document.getElementById('popup2').style.display = 'block';
            // 暂存原颜色
            let bg=document.body.style.backgroundColor;
            // 暂存阅读滚动条
            let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
            let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
            let readHeight = (scrollTop * 1.00)/scrollHeight
            // 修改背景为全透明
            document.body.style.backgroundColor = 'rgba(255, 255, 255, 0)';
            document.getElementById("total-content").style.display='none'

            // 吸色笔
            if ('EyeDropper' in window) {
                // 初始化一个EyeDropper对象
                const eyeDropper = new EyeDropper()
                // 监听按钮的点击事件
                let selector = document.getElementById('selector');
                selector.addEventListener('click', async () => {
                    try {
                        document.body.style.backgroundColor = 'rgba(255, 255, 255, 0)';
                        document.getElementById('popup2').style.display = 'none';
                        // 开始拾取颜色
                        const result = await eyeDropper.open()
                        //显示颜色的值
                        document.getElementById('popup2').style.display = 'block';
                        document.getElementById('color').style.backgroundColor = result.sRGBHex;
                        document.getElementById('codeHEX').value = result.sRGBHex;
                    } catch (e) {
                        console.log('用户取消了取色')
                    }
                })
            }else {
                console.log('取色器不可以使用')
                document.getElementById('selector').style.display = 'none';
            }

            // 改色
            let colorChange = this.document.getElementById('change-color');
            colorChange.onclick = function () {
                if (fontColor){
                    //改变文字颜色
                    document.body.style.color = document.getElementById('codeHEX').value;
                    let elements = document.getElementsByClassName('color-input');
                    for (let i = 0; i < elements.length; i++) {
                        elements[i].style.color = document.getElementById('codeHEX').value;
                    }
                }else {
                    //改变背景颜色
                    document.body.style.backgroundColor = document.getElementById('codeHEX').value;
                    //修改暂存背景色
                    bg=document.body.style.backgroundColor
                }
            }

            // 保存
            let colorSave = this.document.getElementById('save-color');
            colorSave.onclick = function () {
                //遍历查找：存在则直接返回
                for (let i = 0; i < colorData.length; i++) {
                    if (colorData[i].codeHEX === document.getElementById('codeHEX').value) {
                        return
                    }
                }
                //没查到：则添加新颜色
                let newColor = {
                    colorName: document.getElementById('colorName').value,
                    codeHEX: document.getElementById('codeHEX').value,
                }
                colorData.push(newColor)
                //保存在本地json
                if (fontColor){
                    fs.writeFileSync(colorPath, JSON.stringify(colorData, null, '\t'));
                }else {
                    fs.writeFileSync(bgPath, JSON.stringify(colorData, null, '\t'));
                }

                // 添加到右击菜单
                colorMenu.insert(colorMenu.items.length-1,new MenuItem({
                    label: newColor.colorName,
                    click: function () {
                        if (fontColor){
                            document.body.style.color = newColor.codeHEX;
                        }else {
                            document.body.style.backgroundColor = newColor.codeHEX;
                        }
                    }
                }))
            }

            // 关闭
            document.getElementById('close2').addEventListener('click', (e)=> {
                // 隐藏弹窗
                e.stopPropagation();
                document.getElementById('popup2').style.display = 'none'
                // 清空输入框
                document.getElementById('colorName').value = ''
                document.getElementById('codeHEX').value = ''
                document.getElementById('color').style.backgroundColor = '';
                // 恢复原色
                document.body.style.backgroundColor=bg
                // 恢复文字
                document.getElementById("total-content").style.display='block'
                //阅读滚动条
                let scrollHeight2 = document.documentElement.scrollHeight || document.body.scrollHeight
                document.documentElement.scrollTop  = 1.00*readHeight*scrollHeight2
            })
        }
    }))
}
// 初始化颜色/背景菜单
function initColorMenu() {
    // 初始化颜色子菜单
    let colorMenu = new Menu();
    //填充数据，补充按钮
    makeColor(true,colorMenu)
    //添加到菜单
    menuColor = new MenuItem({
        label: '颜色',
        type: 'submenu',
        submenu: colorMenu
    })

    // 初始化背景子菜单
    let bgMenu = new Menu();
    //填充数据，补充按钮
    makeColor(false,bgMenu)
    menuBg = new MenuItem({
        label: '背景',
        type: 'submenu',
        submenu: bgMenu
    })
}

// 初始化页面后获取颜色设置
function InitPageColor() {
    let bgColor = localStorage.getItem('bgColor');
    if (bgColor) {
        document.body.style.backgroundColor=bgColor;
    }
    let fontColor = localStorage.getItem('fontColor');
    if (fontColor) {
        document.body.style.color=fontColor;
        // input内文字
        let inputs = document.querySelectorAll('input');
        inputs.forEach(function(input) {
            input.style.color = fontColor;
        });
        // select框
        let selects = document.querySelectorAll('select');
        selects.forEach(function(select) {
            select.style.color = fontColor;
        });
    }
    let fontSize = localStorage.getItem('fontSize');
    if (fontSize) {
        document.getElementById("content").style.fontSize=fontSize;
    }
    // 可编辑弹窗存在
    if(document.getElementById('popup') != null){
        let pointSize = localStorage.getItem('pointSize');
        if (pointSize) {
            document.getElementById("points").style.fontSize=pointSize;
        }
    }
}

// 在跳转前设置背景颜色并存储到localStorage
function goToPage(url) {
    saveConfig()
    window.location.href = url;
}
function saveConfig() {
    let bgColor = document.body.style.backgroundColor;
    localStorage.setItem('bgColor', bgColor);
    let fontColor = document.body.style.color;
    localStorage.setItem('fontColor', fontColor);
    if (FileName===""){
        let fontSize = document.getElementById("content").style.fontSize;
        localStorage.setItem('fontSize', fontSize);
    }
    // 可编辑弹窗存在
    if(document.getElementById('popup') != null) {
        let pointSize = document.getElementById("points").style.fontSize;
        localStorage.setItem('pointSize', pointSize);
    }
}
//全局查找
let findInPage = new FindInPage(remote.getCurrentWebContents(), {
    preload: true,
    offsetTop: 6,
    offsetRight: 10
})
ipcRenderer.on('on-find', (e, args) => {
    findInPage.openFindWindow()
})

//放大
ipcRenderer.on('big', (e, args) => {
    let element = document.getElementById("content");
    // 弹窗存在且出于编辑状态
    if(document.getElementById('popup') != null){
        if (document.getElementById('popup').style.display==='block'){
            element = document.getElementById('points');
        }
    }
    // 获取当前字体大小
    let style = window.getComputedStyle(element, null).getPropertyValue('font-size');
    let fontSize = parseFloat(style);
    // 获取滚动条位置
    // 1滚动条据上距离
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    // 2整个滚动条长度
    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
    // 改变字体大小
    element.style.fontSize = (fontSize + 1) + 'px';
    // 3获取改变字体后的滚动条长度
    let scrollHeight2 = document.documentElement.scrollHeight || document.body.scrollHeight
    // 恢复滚动条位置：由于p标签的高度是动态的，缩放导致的换行使得百分比计算的定位可能有偏差
    document.documentElement.scrollTop= ((scrollTop * 1.00)/scrollHeight)*scrollHeight2
})
//缩小
ipcRenderer.on('sm', (e, args) => {
    let element = document.getElementById("content");
    // 弹窗存在且出于编辑状态
    if(document.getElementById('popup') != null){
        if (document.getElementById('popup').style.display==='block'){
            element = document.getElementById('points');
        }
    }
    let style = window.getComputedStyle(element, null).getPropertyValue('font-size');
    let fontSize = parseFloat(style);
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight

    element.style.fontSize = (fontSize - 1) + 'px';

    let scrollHeight2 = document.documentElement.scrollHeight || document.body.scrollHeight
    document.documentElement.scrollTop= ((scrollTop * 1.00)/scrollHeight)*scrollHeight2
})

//读取文件后渲染
ipcRenderer.on("file-path", (e, args) => {
    let path = args.split('\\')
    document.getElementById("file-name").innerText=path[path.length-1]
    document.getElementById("file-name").name=args
})