var ReadWriterPath=""

window.onload = function(){
    // 获取颜色设置
    InitPageColor()

    //选择文件
    let btn0 = document.getElementById('file')
    btn0.onclick = function(){
        ipcRenderer.send("select-file");
    }

    let writer =  document.querySelector('#content')
    // 打开文件
    let btn1 = this.document.querySelector('#btn1')
    btn1.onclick=function () {
        //文件名
        if (document.getElementById("file-name").innerText!==""){
            ReadWriterPath = document.getElementById("file-name").name
        }else {
            ReadWriterPath = writerPath
        }
        // 读取文件内容
        fs.readFile(ReadWriterPath, 'utf8', (err, dataSrc) => {
            // 将文件内容按行分割 \n换行 \r回车
            // 记事本\r\n
            // unix是\n windows是\n\r mac是\r
            let str = dataSrc.replaceAll('\r','');
            let lines = str.split('\n');
            // 逐行处理并替换
            let modifiedLines = lines.map(line => {
                if(line===""){
                    return "<div><br></div>"
                }else {
                    return "<div>"+line+"</div>"
                }
            });
            // 将处理后的行拼接成字符串
            let modifiedData = modifiedLines.join("");
            writer.innerHTML = modifiedData
            // 滚动到最底
            document.documentElement.scrollTop = document.documentElement.scrollHeight;
            //字数统计
            countNums()
        })
        document.getElementById("file-name").innerText=""
        document.getElementById("file-name").name=""
    }

    //手动保存
    let btn2 = this.document.querySelector('#saveFile')
    btn2.onclick=function() {
        ipcRenderer.send("save-file",ReadWriterPath);
    }

    //监听右键
    window.addEventListener("contextmenu",(e)=>{
        e.preventDefault() //阻止默认行为
        //弹出右键菜单
        mainMenu.popup({
            window:remote.getCurrentWindow()
        })
    })

    //监听文本改变
    writer.addEventListener('input', function (){
        // 第一行是否被包裹
        let firstLineEndIndex = this.innerHTML.indexOf('<div>');
        if (firstLineEndIndex === -1) {
            this.innerHTML = '<div>' + this.innerHTML.substring(0, this.innerHTML.length) + '</div>';
        }
        //自动保存
        let stringData = writer.innerText.replace(/\n\n/g,"\r").replace(/\n/g,"\r").replace(/\r/g,"\r\n")
        fs.writeFile(backupPath, stringData, 'utf8', function(err){
            if(err) throw err;
        });
        fs.writeFile(writerPath, stringData, 'utf8', function(err){
            if(err) throw err;
        });
        //字数统计
        countNums()
    })

    //统计鼠标选中文本的字数
    document.addEventListener('mouseup', function() {
        //选中字数
        let selection = window.getSelection() || document.getSelection();
        let text = selection.toString();
        let counter=text.replace(/\n|\r/g, '').length;
        //整体字数
        let text2=document.querySelector('#content').innerText;
        let counter2=text2.replace(/\n|\r/g, '').length;
        //修改统计器
        let wordCount = document.getElementById('wordCount');
        if (counter!==0){
            wordCount.textContent = counter + "/" + counter2;
        }else {
            wordCount.textContent = counter2;
        }
    });
}

//回到顶部
ipcRenderer.on('to-top', (e, args) => {
    document.documentElement.scrollTop=0
})
ipcRenderer.on('to-end', (e, args) => {
    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
    document.documentElement.scrollTop=scrollHeight
})
ipcRenderer.on("save-dialog", (e, args) => {
    // 要保存的字符串
    let writer =  document.querySelector('#content')
    let stringData = writer.innerText.replace(/\n\n/g,"\r").replace(/\n/g,"\r").replace(/\r/g,"\r\n")
    fs.writeFileSync(args, stringData, 'utf8', function(err){
        if(err) throw err;
    });
})

// 保存设置
ipcRenderer.on('save', (e, args) => {
    saveConfig()

})

//字数统计
function countNums() {
    let text=document.querySelector('#content').innerText;
    let counter=text.replace(/\n|\r/g, '').length;
    let wordCount = document.getElementById('wordCount');
    wordCount.textContent = counter;
}

// 设定速记
ipcRenderer.on('share-points', (e, args) => {
    let points =  document.querySelector('#points');
    // 读取本地
    fs.readFile(writerPointPath, 'utf8', (err, dataSrc) => {
        let str = dataSrc.replaceAll('\r','');
        let lines = str.split('\n');
        // 逐行处理并替换
        let modifiedLines = lines.map(line => {
            if(line===""){
                return "<div><br></div>"
            }else {
                return "<div>"+line+"</div>"
            }
        });
        // 将处理后的行拼接成字符串
        let modifiedData = modifiedLines.join("");
        // 显示内容
        points.innerHTML = modifiedData
    })

    // 显示弹窗
    if (document.body.style.backgroundColor==='rgba(255, 255, 255, 0)'||document.body.style.backgroundColor===''){
        document.getElementById('popup-content').style.backgroundColor='#ffffff';
    }else {
        document.getElementById('popup-content').style.backgroundColor=document.body.style.backgroundColor;
    }
    document.getElementById('popup').style.display = 'block';

    // 监听输入框
    points.addEventListener('input', function (){
        // 获取内容
        let stringData = points.innerText.replace(/\n\n/g,"\r").replace(/\n/g,"\r").replace(/\r/g,"\r\n")
        // 保存
        fs.writeFileSync(writerPointPath, stringData);
    })

    // 监听关闭按钮
    document.getElementById('close').addEventListener('click', (e)=> {
        //隐藏弹窗
        e.stopPropagation();
        document.getElementById('popup').style.display = 'none'
    })
})