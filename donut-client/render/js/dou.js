var DouHeight= 0.0
var GroupId = ""
var TopicGroup = ""
var TopicTitle = ""
var TopicUrl = ""

// 初始化
window.onload = function() {
    // 初始化首页并渲染
    initIndex()

    // 获取颜色设置
    InitPageColor()

    // 监听右键
    window.addEventListener("contextmenu",(e)=>{
        // 阻止默认行为
        e.preventDefault()

        // 检查目标元素是否具有特定的类名或属性，以此来判断是否是特定元素
        // 获取触发事件的元素
        let targetElement = e.target;
        // 本元素/母元素包含收藏的话题
        if (targetElement.classList.contains('dou-like-title')||targetElement.parentElement.classList.contains('dou-like-title')) {
            // 如果是要排除的元素，则不做任何处理，即不弹出菜单
            return;
        }

        // 弹出右键菜单
        mainMenu.popup({
            window:remote.getCurrentWindow()
        })
    })

    // 监听返回
    let toGroup = document.getElementById('to-group');
    toGroup.addEventListener('click', (e)=> {
        let discussionDiv = document.getElementById('discussion');
        // 转到讨论页
        discussionDiv.scrollIntoView();
        let scrollNow = document.documentElement.scrollTop;
        let scrollAdd = 1.00 * DouHeight * discussionDiv.getBoundingClientRect().height;
        // 恢复点击话题前的高度
        document.documentElement.scrollTop = scrollNow - scrollAdd;
    })

    // 修改 cookie
    let cookieInput =  document.querySelector('#cookie-input');
    // 1.显示 cookie
    document.getElementById('change-cookie').addEventListener('click', (e)=>{
        // 读取 cookie
        let configData = JSON.parse(fs.readFileSync(configPath, 'utf8'));
        // 显示内容
        cookieInput.value = configData.douCookie;
        document.getElementById('dou-cookie').style.display = 'block';
    })
    // 2.保存 douCookie
    document.getElementById('save-cookie').addEventListener('click', (e)=> {
        // 读取 cookie
        let configData = JSON.parse(fs.readFileSync(configPath, 'utf8'));
        // 获取内容
        configData.douCookie=cookieInput.value
        //保存在本地json
        fs.writeFileSync(configPath, JSON.stringify(configData,null, '\t'));
        // 隐藏
        document.getElementById('dou-cookie').style.display = 'none';
    })

    // 关键字搜索
    document.getElementById('query-input').addEventListener('keyup', function(event) {
        event.preventDefault();
        // 检查按键是否是回车键
        if (event.keyCode === 13) {
            let q = this.value;
            search(q)
        }
    })
    document.getElementById('query').addEventListener('click',(e)=>{
        let q = document.getElementById('query-input').value
        search(q)
    })
}

// 获取小组讨论，并渲染到首页
function initIndex(){
    // 渲染收藏
    let likeBtn = document.getElementById("like-topic");
    likeBtn.onclick=function () {
        showLikeTopic(likeBtn);
    }
    // 渲染小组
    let groupData = JSON.parse(fs.readFileSync(groupPath, 'utf8'));
    let groupContent = document.getElementById("group");
    // 遍历小组
    for (let i = 0; i < groupData.length; i++) {
        let group = document.createElement("div");
        group.className = 'shelf'
        group.innerHTML = groupData[i].name;
        // 获取话题列表
        group.onclick=function(){
            GroupId = groupData[i].groupId
            TopicGroup=groupData[i].name
            // 移除其他小组点击效果
            let menuItems = document.querySelectorAll(".shelfActive");
            for(let j = 0; j < menuItems.length; j++) {
                menuItems[j].className='shelf'
            }
            group.className='shelfActive'
            let url= "https://www.douban.com/group/"+groupData[i].groupId+"/discussion?start=0&type=new"
            getDouList(url);
        }
        groupContent.append(group)
    }
    // 默认显示收藏的帖子
    showLikeTopic(likeBtn);
}

function showLikeTopic(likeBtn) {
    GroupId = ""
    TopicGroup=""
    // 移除其他小组点击效果
    let menuItems = document.querySelectorAll(".shelfActive");
    for(let j = 0; j < menuItems.length; j++) {
        menuItems[j].className='shelf'
    }
    likeBtn.className='shelfActive'
    // 获取收藏帖子
    let topicData = JSON.parse(fs.readFileSync(topicPath, 'utf8'));
    // 渲染
    let discussionContent = document.getElementById("discussion");
    discussionContent.innerHTML="<p><span class='dou-like-group'>小组</span><span class='dou-like-title'>讨论&nbsp;</span></p>"
    for (let i= 0; i < topicData.length; i++) {
        let group =topicData[i]//修复删除收藏后跳转不一致的问题
        let topic = document.createElement("p");
        topic.className='like-topic'
        // 小组
        let span1 = document.createElement("span");
        span1.className='dou-like-group'
        span1.innerText=group.group
        topic.append(span1)
        // 标题
        let span2 = document.createElement("span");
        span2.innerHTML=group.title
        span2.title=span2.innerText
        span2.className='dou-like-title'
        // 左击获取内容
        span2.onclick=function () {
            getDouTopic(group.title, group.href)
        }
        topic.append(span2)
        discussionContent.append(topic)
        //  右击删除收藏
        span2.oncontextmenu = function(){
            // 删除
            span1.innerText = '删除';
            span1.className = 'dou-delete';
            span2.style.pointerEvents = 'none'
            span1.onclick=function(){
                topicData.splice(topicData.indexOf(group), 1);
                //保存到本地json
                fs.writeFileSync(topicPath, JSON.stringify(topicData,null, '\t'));
                //移除页面
                topic.remove()
            }

            //3s后还原
            setTimeout(function () {
                span1.onclick=null
                span1.className='dou-like-group'
                span1.innerText=group.group
                span2.style.pointerEvents = null
            },3000);
        }
    }
}

//获取小组讨论
function getDouList(url) {
    let options = {
        url:"http://localhost:8708/dou/list",
        data: {
            "url": url,
            "userData":dataPath,
        },
        method: 'post',
        headers:{
            'Content-Type':'multipart/form-data'
        }
    }
    //发送请求，获取数据并渲染
    axios(options).then(
        (res)=>{
            if (res.data.code!==200){
                console.log(res.data.msg)
                ipcRenderer.invoke('logger', 'error', 'dou/list 渲染失败:'+res.data.msg)
                return
            }
            // 渲染
            let discussionContent = document.getElementById("discussion");
            discussionContent.innerHTML="<div id='toPage'><div>"+res.data.body+"<br>"
            document.getElementById('toPage').scrollIntoView();
        }
    ).catch(
        (error)=>{
            console.log('getDouList 渲染失败:',error)
            ipcRenderer.invoke('logger', 'error', 'getDouList 渲染失败:'+error)
        }
    )
}

function getDouTopic(title,url){
    let options = {
        url:"http://localhost:8708/dou/topic",
        data: {
            "url": url,
            "userData":dataPath,
        },
        method: 'post',
        headers:{
            'Content-Type':'multipart/form-data'
        }
    }
    //发送请求，获取数据并渲染
    axios(options).then(
        (res)=>{
            if (res.data.code!==200){
                console.log(res.data.msg)
                ipcRenderer.invoke('logger', 'error', 'dou/topic 渲染失败:'+res.data.msg)
                return
            }
            //讨论列表
            let discussionDiv = document.getElementById('discussion').getBoundingClientRect();
            //整体高度
            let discussionHeight = discussionDiv.height
            //头部距离可视窗高度
            let discussionTop = discussionDiv.top;
            //保存比例(是从列表点进来的，而不是下一页)
            if ((0-discussionTop)< discussionHeight){
                DouHeight = (discussionTop * 1.00) / discussionHeight
            }
            // 渲染
            let topicContent = document.getElementById("topic");
            topicContent.innerHTML="<div id='toTopic'></div>"+res.data.body+"<br>"
            document.getElementById('toTopic').scrollIntoView();
            // 显示返回键
            let toGroup = document.getElementById('to-group');
            // 暂存贴子标题url
            TopicTitle = title
            TopicUrl = url
            if (TopicGroup===""){
                TopicGroup=res.data.group
            }
        }
    ).catch(
        (error)=>{
            console.log('getDouList 渲染失败:',error)
            ipcRenderer.invoke('logger', 'error', 'getDouList 渲染失败:'+error)
        }
    )
}

//关键字搜索
function search(q) {
    // 若关键字为空则不搜索
    if (q===""){
        return
    }
    let url = "https://www.douban.com/group/search?cat=1013"
    if (GroupId!==""){
        url=url+"&group="+GroupId
    }
    url=url+"&q="+q
    let sort = document.getElementById('query-sort').value;
    if (sort!==""){
        url=url+"&sort="+sort
    }else {
        // 默认按时间排序
        url=url+"&sort=time"
    }
    getDouList(url);
}

// 豆瓣嘚不嘚
ipcRenderer.on('share-points', (e, args) => {
    let points =  document.querySelector('#points');
    // 读取本地
    fs.readFile(douPointPath, 'utf8', (err, dataSrc) => {
        let str = dataSrc.replaceAll('\r','');
        let lines = str.split('\n');
        // 逐行处理并替换
        let modifiedLines = lines.map(line => {
            if(line===""){
                return "<div><br></div>"
            }else {
                return "<div>"+line+"</div>"
            }
        });
        // 将处理后的行拼接成字符串
        let modifiedData = modifiedLines.join("");
        // 显示内容
        points.innerHTML = modifiedData
    })

    // 显示弹窗
    if (document.body.style.backgroundColor==='rgba(255, 255, 255, 0)'||document.body.style.backgroundColor===''){
        document.getElementById('popup-content').style.backgroundColor='#ffffff';
    }else {
        document.getElementById('popup-content').style.backgroundColor=document.body.style.backgroundColor;
    }
    document.getElementById('popup').style.display = 'block';

    // 监听输入框
    points.addEventListener('input', function (){
        // 获取内容
        let stringData = points.innerText.replace(/\n\n/g,"\r").replace(/\n/g,"\r").replace(/\r/g,"\r\n")
        // 保存
        fs.writeFileSync(douPointPath, stringData);
    })

    // 监听关闭按钮
    document.getElementById('close').addEventListener('click', (e)=> {
        //隐藏弹窗
        e.stopPropagation();
        document.getElementById('popup').style.display = 'none'
    })
})

//刷新
ipcRenderer.on('refresh', (e, args) => {
    // 保存配置
    saveConfig()
    // 返回顶部
    document.documentElement.scrollTop=0
    // 刷新页面
    location.reload();
})
//回到顶部
ipcRenderer.on('to-top', (e, args) => {
    let topicElement = document.getElementById('topic')
    //讨论列表
    let topicDiv = topicElement.getBoundingClientRect();
    //头部距离可视窗高度
    let topicTop = topicDiv.top;

    // 处于话题页
    if (topicTop < 0){
        // 回到标题
        topicElement.scrollIntoView()
    }else {
        // 回到谈论列表
        document.documentElement.scrollTop=0
    }
})

ipcRenderer.on('to-end', (e, args) => {
    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
    document.documentElement.scrollTop=scrollHeight
})
// 保存设置
ipcRenderer.on('save', (e, args) => {
    saveConfig()
})
// 收藏帖子
ipcRenderer.on('like-topic', (e, args) => {
    //没有点击帖子 TopicUrl为空
    if (TopicUrl===""){
        return;
    }
    // 获取收藏帖子
    let topicData = JSON.parse(fs.readFileSync(topicPath, 'utf8'));
    // 查找是否收藏过
    for (let i = 0; i < topicData.length; i++) {
        if (topicData[i].href === TopicUrl) {
            return;
        }
    }
    // 新收藏
    let newTopic = {
        group: TopicGroup,
        title: TopicTitle,
        href: TopicUrl,
    }
    topicData.unshift(newTopic)

    //保存在本地json
    fs.writeFileSync(topicPath, JSON.stringify(topicData, null, '\t'));
})