//自动翻页定时器
var ref
var scrollSpeed=50
// 当前应用规则的目录class名 chapter[id] 的id
var dirId = 0
// 获取目前打开的本地文件绝对路径
var BookPath=""
// 书架菜单
var menuShelf
// 目录菜单
var menuDir
// 自动获取网络小说下一章的定时器
var getNextInterval
// 获取网络小说章节
var onlineChapters
// 获取分组的tag
var shelfMap
// 弹幕
var duangData

// 初始化
window.onload = function(){
    // 初始化首页并渲染
    initIndex()

    // 获取颜色设置
    InitPageColor()

    // 上传并解析文档
    let btn1 = this.document.querySelector('#btn1')
    //读取文件
    btn1.onclick = function(){
        //选择文件
        if (document.getElementById("file-name").innerText!=="") {
            FileName = document.getElementById("file-name").innerText
            BookPath = document.getElementById("file-name").name
            getStory(BookPath)
        }else {
            getStory("")
        }
    }

    // 监听右键
    window.addEventListener("contextmenu",(e)=>{
        // 阻止默认行为
        e.preventDefault()

        // 检查目标元素是否具有特定的类名或属性，以此来判断是否是特定元素
        // 获取触发事件的元素
        let targetElement = e.target;
        // 排除可右击元素的菜单事件
        if (targetElement.closest('.book, .shelf, .shelfActive')) {
            // 如果是要排除的元素，则不做任何处理，即不弹出菜单
            return;
        }

        // 弹出右键菜单
        mainMenu.popup({
            window:remote.getCurrentWindow()
        })
    })

    // 按分组导出
    let btn2 = this.document.querySelector('#btn2')
    btn2.onclick = function(){
        //创建目录文件夹
        let parentDir = bookPath+"output/"

        //读取分组
        let shelfData = JSON.parse(fs.readFileSync(shelfPath, 'utf8'));
        //目录路径映射
        let groupIdToPathMap = new Map();
        createDirMap(groupIdToPathMap,shelfData,parentDir)

        //获取阅读记录
        let readData = JSON.parse(fs.readFileSync(readPath, 'utf8'));
        //复制到分组文件夹
        readData.forEach(file => {
            // 忽略网络小说
            if (file.online){
                return
            }
            // 复制
            let targetDir = groupIdToPathMap.get(file.groupId);
            if (targetDir) {
                let targetFilePath = path.join(targetDir, file.fileName);
                fs.copyFile(file.bookPath, targetFilePath, (err) => {
                    if (err) {
                        console.error('文件复制失败:', err);
                        ipcRenderer.invoke('logger', 'error', '文件复制失败:'+err)
                    } else {
                        console.log('文件复制成功:', targetFilePath);
                    }
                });
            } else {
                console.error('未找到对应的目录路径:', file.groupId);
                ipcRenderer.invoke('logger', 'error', '未找到文件'+file.fileName+'的目录'+file.groupId)
            }
        });
    }

    // 按分组移动
    let btn3 = this.document.querySelector('#btn3')
    btn3.onclick = function(){
        //读取分组
        let shelfData = JSON.parse(fs.readFileSync(shelfPath, 'utf8'));
        //创建目录，并获取路径映射
        let groupIdToPathMap = new Map();
        createDirMap(groupIdToPathMap,shelfData,bookPath)

        //获取阅读记录
        let readData = JSON.parse(fs.readFileSync(readPath, 'utf8'));

        // 遍历所有阅读记录
        for (let j = 0; j < readData.length; j++) {
            // 忽略网络小说
            if (readData[j].online) {
                continue;
            }
            //获取目的文件夹
            let targetDir = groupIdToPathMap.get(readData[j].groupId);
            // 移动文件到该目录
            let targetFilePath = path.join(targetDir, readData[j].fileName);
            if (readData[j].bookPath!==targetFilePath) {
                try {
                    fs.renameSync(readData[j].bookPath, targetFilePath);
                    // 修改路径
                    readData[j].bookPath = targetFilePath
                    // ipcRenderer.invoke('logger', 'info', '成功移动到'+targetDir)
                } catch (err) {
                    console.log('文件移动失败:', err)
                    ipcRenderer.invoke('logger', 'error', '文件移动失败:' + err)
                }
            }
            console.log(readData[j])
        }
        // 保存
        fs.writeFileSync(readPath, JSON.stringify(readData,null, '\t'));
        // 刷新首页
        saveConfig()
        location.reload()
    }
}
//递归创建层级目录并将路径存储到映射中
function createDirMap(groupIdToPathMap,groups, currentDir) {
    groups.forEach(group => {
        // 忽略系统分组 全部、已分类
        if (group.groupId<0){
            return;
        }
        // 构建完整的文件夹路径
        let newDir = path.join(currentDir, group.groupName);
        groupIdToPathMap.set(group.groupId, newDir);
        //创建文件
        if (!fs.existsSync(newDir)) {
            fs.mkdirSync(newDir, { recursive: true });
        }
        // 如果有下级，递归创建
        if (group.children) {
            createDirMap(groupIdToPathMap,group.children, newDir);
        }
    });
}


//选择文件
let btn0 = document.getElementById('file')
btn0.onclick = function(){
    ipcRenderer.send("select-file");
}

// 初始化右击书架菜单：menuShelf
function initShelfMenu() {
    // 初始化书架子菜单
    let shelfMenu = new Menu();
    //读取书架
    let shelfData = JSON.parse(fs.readFileSync(shelfPath, 'utf8'));
    let readData = JSON.parse(fs.readFileSync(readPath, 'utf8'));
    for (let i = 0; i < shelfData.length; i++) {
        let groupMenu = new Menu();
        // 默认分组不显示
        if (shelfData[i].groupId<0){
            continue
        }
        // 移动分组
        groupMenu.append(new MenuItem({
                label: '移动到该分组',
                click: function () {
                    moveBook(readData,shelfData[i].groupId)
                }
            }))
        // 前8本
        let maxMenu =8
        if (shelfData[i].children!=null){
            //  二级分类
            addNextGroup(readData,shelfData[i].children,groupMenu)

            //  菜单数量
            if (shelfData[i].children.length>4){
                maxMenu=4//若下层分类大于四个，则保留外层的四本书
            }else {
                maxMenu-=shelfData[i].children.length//若下层分类小于等于四个，则一共保留8个
            }
        }
        let z=0;
        for (let j = 0; j <readData.length && maxMenu>0; j++) {
            if (shelfData[i].groupId===-1||readData[j].groupId===shelfData[i].groupId) {
                groupMenu.append(new MenuItem({
                    label: readData[j].bookName,
                    click: function () {
                        // 保存上一个阅读记录
                        save()
                        // 文件名
                        FileName = readData[j].fileName
                        BookPath = readData[j].bookPath
                        // 渲染
                        if (readData[j].online){
                            // 渲染网络书籍
                            getOnlineNovel(readData[j])
                        }else {
                            // 渲染本地书籍
                            getStory(BookPath)
                        }
                    }
                }))
                maxMenu--
            }
        }
        // 添加到子菜单
        shelfMenu.append(new MenuItem({
            label:shelfData[i].groupName,
            type: 'submenu',
            submenu:groupMenu
        }))
    }
    menuShelf =new MenuItem({
        label:'书架',
        type: 'submenu',
        submenu: shelfMenu
    })
}
//移动书到其他书架
function moveBook(readData,toGroupId){
    if (FileName===""){
        // alert('未选择文件')
        return
    }
    // 遍历找到该书并修改分组
    for (let j = 0; j < readData.length; j++) {
        if (readData[j].fileName===FileName) {
            // 修改分组
            readData[j].groupId = toGroupId
            fs.writeFileSync(readPath, JSON.stringify(readData,null, '\t'));
            // 保存阅读记录
            save()
            // 刷新书架菜单
            initShelfMenu()
            //清空主菜单
            mainMenu = new Menu();
            //添加到菜单组
            mainMenu.append(menuDir)
            mainMenu.append(menuColor)
            mainMenu.append(menuBg)
            mainMenu.append(menuScroll)
            mainMenu.append(menuShelf)
            // alert('已将'+FileName.substring(0,FileName.lastIndexOf("."))+'添加到分组'+toGroupId)
            return;
        }
    }
    // 没找到，则插入记录
    // TODO 网络未添加
    let tmp=FileName.substring(0,FileName.lastIndexOf("."))
    let reg=/(((作者[:：])|(by)|(BY)|—|-|》|~|～).*)|《| /g

    //字号
    let element = document.getElementById("content");
    let style = window.getComputedStyle(element, null).getPropertyValue('font-size');
    let fontSize = parseFloat(style) + 'px';
    //修改时间
    let readTime = new Date().getTime()

    //滚动条百分比距离
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
    let scroll = (scrollTop * 1.00)/scrollHeight

    let newFile = {
        fileName: FileName,
        fontSize: fontSize,
        readHeight: scroll,
        readTime: readTime,
        dirId: dirId,
        groupId: toGroupId,
        bookName: tmp.replace(reg,''),
        bookPath: BookPath,
    }
    readData.push(newFile)
    //保存在本地json
    fs.writeFileSync(readPath, JSON.stringify(readData, null, '\t'));
    // 刷新右击菜单 重新读取渲染
    getStory(BookPath)
}
//获取菜单合集(递归)
function addNextGroup(readData,children,groupMenu){
    if (children==null){
        return
    }
    for (let z = 0; z < children.length; z++) {
        let groupMenuChildren = new Menu();
        // 移动分组
        groupMenuChildren.append(new MenuItem({
            label: '移动到该分组',
            click: function () {
                moveBook(readData,children[z].groupId)
            }
        }))

        //  菜单数量
        let maxMenu =8
        if (children[z].children!=null){
            //  N级分类
            addNextGroup(readData,children[z].children,groupMenuChildren)

            //  菜单数量
            if (children[z].children.length>4){
                maxMenu=4//若下层分类大于四个，则保留外层的四本书
            }else {
                maxMenu-=children[z].children.length//若下层分类小于等于四个，则一共保留8个
            }
        }

        for (let j = 0; j <readData.length && maxMenu>0; j++) {
            if (readData[j].groupId===children[z].groupId) {
                groupMenuChildren.append(new MenuItem({
                    label: readData[j].bookName,
                    click: function () {
                        // 保存上一个阅读记录
                        save()
                        // 文件名
                        FileName = readData[j].fileName
                        BookPath = readData[j].bookPath
                        // 渲染
                        if (readData[j].online){
                            // 渲染网络书籍
                            getOnlineNovel(readData[j])
                        }else {
                            // 渲染本地书籍
                            getStory(BookPath)
                        }
                    }
                }))
                maxMenu--
            }
        }
        // 添加到子菜单
        groupMenu.append(new MenuItem({
            label:children[z].groupName,
            type: 'submenu',
            submenu:groupMenuChildren
        }))
    }
}

// 初始化首页
function initIndex() {
    let shelfData = JSON.parse(fs.readFileSync(shelfPath, 'utf8'));
    // 使用递归函数处理数据(嵌套分组展开)
    shelfMap = flattenGroups(shelfData);
    // 遍历书架分组
    let shelfDiv = document.getElementById("shelf");
    let bookContent = document.getElementById("book");
    for (let i = 0; i < shelfData.length; i++) {
        // 分组名
        let shelf = document.createElement("div");
        shelf.className = 'shelf'
        shelf.innerHTML = shelfData[i].groupName;
        if (i===0){
            shelf.className='shelfActive'
        }
        // 左击显示分组下的书
        shelf.onclick=function(){
            // 移除其他分组显示效果
            let menuItems = document.querySelectorAll(".shelfActive");
            for(let j = 0; j < menuItems.length; j++) {
                menuItems[j].className='shelf'
            }
            shelf.className='shelfActive'
            // 移除筛选
            let shelfChildren = document.getElementById("shelf-filter");
            shelfChildren.innerHTML=""
            // 获取书籍
            getShelfBooks(bookContent,shelfData[i]);
        }
        // 右击显示女分类
        shelf.oncontextmenu = function(){
            //选中状态下才会触发
            if (shelf.className!=='shelfActive'){
                return;
            }
            // 如果存在元素：显示则隐藏，隐藏则显示
            let shelfChildren = document.getElementById("shelf-filter");
            if (shelfChildren.children.length > 0) {
                if (window.getComputedStyle(shelfChildren).display === 'none') {
                    shelfChildren.style.display = 'block'; // 显示
                } else {
                    shelfChildren.style.display = 'none'; // 隐藏
                }
                return;
            }
            // 获取女分类
            let dirList = [shelfData[i].groupId]
            getDirs(shelfData[i].children,dirList)
            if (dirList.length===1){
                return;
            }
            // 提示用户
            let shelfSelect = document.createElement("span");
            shelfSelect.innerText='筛选 '
            shelfChildren.append(shelfSelect);
            // 全部/没有女分组的则不用再筛选
            for(let j = 0; j < dirList.length; j++) {
                // 添加孩子分类
                let shelfChild = document.createElement("div");
                shelfChild.className ='shelf'
                shelfChild.setAttribute('groupId',dirList[j])
                shelfChild.innerHTML = shelfMap[dirList[j]]
                //监听点击事件，若点击则筛选
                shelfChild.onclick=function(){
                    if (shelfChild.className ==='shelf'){
                        //筛选
                        shelfChild.className ='shelfActive'
                    }else {
                        //清除筛选
                        shelfChild.className ='shelf'
                    }
                    // 获取#shelf-filter下所有.shelfActive的groupId
                    let groupIds = [...new Set([...document.querySelectorAll('#shelf-filter .shelfActive')]
                        .filter(div => div.hasAttribute('groupId'))
                        .map(div => div.getAttribute('groupId'))//去重
                    )];
                    bookContent.querySelectorAll('div').forEach(div => {
                        // 获取当前 div 的所有 class
                        let classList = div.classList;
                        // 检查 div 的 class 是否包含在 groupIds 中
                        let shouldDisplay = Array.from(classList).some(className => groupIds.includes(className));
                        // 根据是否包含 groupIds 显示或隐藏
                        div.style.display = shouldDisplay ? 'inline-block' : 'none';
                    });
                }
                shelfChildren.append(shelfChild);
            }

        }
        shelfDiv.append(shelf)
    }
    // 默认显示第一个书架（按 bookShelf 中的顺序）
    getShelfBooks(bookContent,shelfData[0]);
}
// 递归函数来处理嵌套的JSON数据：shelfTags[groupId]=groupName
function flattenGroups(groups, result = {}) {
    groups.forEach(group => {
        result[group.groupId] = group.groupName;
        if (group.children && group.children.length > 0) {
            flattenGroups(group.children, result);
        }
    });
    return result;
}
// 获取书架书籍，并渲染到首页
function getShelfBooks(bookContent,shelfData){
    let readData = JSON.parse(fs.readFileSync(readPath, 'utf8'));
    //按时间倒叙
    let sortData = sortJsonArr(readData, "desc", "readTime");
    //保存到本地json
    fs.writeFileSync(readPath, JSON.stringify(sortData,null, '\t'));
    //获取合集
    let dirList = [shelfData.groupId]
    getDirs(shelfData.children,dirList)
    bookContent.innerHTML=''
    for (let j = 0; j < sortData.length; j++) {
        let bookData = sortData[j]//防止删除后移位
        // -1全部 -2已分类
        if (shelfData.groupId < 0 || dirList.includes(bookData.groupId)) {
            // 已分类
            if (shelfData.groupId === -2){
                if (bookData.groupId === 0){
                    continue
                }
            }
            let bookDiv = document.createElement("div");
            bookDiv.className = 'book '+bookData.groupId
            // 分组tag
            if (shelfData.groupId!==bookData.groupId){
                let tag = document.createElement("span");
                tag.className="book-tag"
                tag.innerHTML = shelfMap[bookData.groupId];
                bookDiv.appendChild(tag);
            }
            // 点击后检索书
            let book = document.createElement("span");
            book.innerHTML = bookData.bookName;
            book.className = 'cursor-p'
            // 左击打开文件
            book.onclick=function(){
                // 保存设置
                saveConfig()
                // 文件名
                FileName = bookData.fileName
                BookPath = bookData.bookPath
                if (bookData.online){
                    // 渲染网络书籍
                    getOnlineNovel(bookData)
                }else {
                    // 渲染本地书籍
                    getStory(BookPath)
                }
            }
            // 右击删除文件及历史记录
            book.oncontextmenu = function(){
                // 检查是否已经存在delDiv，如果存在则移除
                let existingDelDiv = bookDiv.querySelector('.del-div');
                if (existingDelDiv) {
                    return
                }

                let delDiv = document.createElement("div");
                delDiv.className = 'del-div'; // 给delDiv添加一个类名
                bookDiv.append(delDiv)

                // 删除
                let yesBtn = document.createElement("div");
                yesBtn.className = 'del-btn'
                yesBtn.innerHTML = '删除';
                yesBtn.title = '删除本地文件';
                yesBtn.onclick=function(){
                    // 删除本地
                    fs.unlink(bookData.bookPath, (err) => {
                        if (err) {
                            console.error('无法删除文件', err);
                            ipcRenderer.invoke('logger', 'error', '无法删除文件:'+err)
                        } else {
                            console.log('文件已成功删除');
                        }
                    });

                    // 移除历史记录
                    sortData.splice(sortData.indexOf(bookData), 1);
                    //保存到本地json
                    fs.writeFileSync(readPath, JSON.stringify(sortData,null, '\t'));

                    // 移除页面
                    bookDiv.remove()
                }
                delDiv.append(yesBtn);

                // 只移除历史记录
                let noBtn = document.createElement("div");
                noBtn.className = 'del-btn'
                noBtn.innerHTML = '移除';
                noBtn.title = '删除阅读记录';
                noBtn.onclick=function(){
                    // 移除历史记录
                    sortData.splice(sortData.indexOf(bookData), 1);
                    //保存到本地json
                    fs.writeFileSync(readPath, JSON.stringify(sortData,null, '\t'));

                    // 移除页面
                    bookDiv.remove()
                }
                delDiv.append(noBtn);

                // 编辑
                let editBtn = document.createElement("div");
                editBtn.className = 'del-btn'
                editBtn.innerHTML = '编辑';
                editBtn.title = '重命名';
                editBtn.onclick=function(){
                    //移除书架状态
                    delDiv.remove()
                    // 显示内容
                    let editElement =  document.querySelector('#edit-input');
                    let type=path.extname(bookData.fileName)
                    editElement.value = path.basename(bookData.fileName,type);
                    // 显示弹窗
                    if (document.body.style.backgroundColor==='rgba(255, 255, 255, 0)'||document.body.style.backgroundColor===''){
                        document.getElementById('popup-content3').style.backgroundColor='#ffffff';
                    }else {
                        document.getElementById('popup-content3').style.backgroundColor=document.body.style.backgroundColor;
                    }
                    document.getElementById('popup3').style.display = 'block';

                    // 保存按钮
                    let editBtn = document.querySelector('#save-edit')
                    editBtn.onclick = function(){
                        renameBook(type,editElement.value,bookData,sortData,bookContent,shelfData)
                    }
                    // 回车等效保存
                    document.getElementById('edit-input').addEventListener('keyup', function(event) {
                        event.preventDefault();
                        // 检查按键是否是回车键
                        if (event.keyCode === 13) {
                            renameBook(type,editElement.value,bookData,sortData,bookContent,shelfData)
                        }
                    })

                    // 监听关闭按钮
                    document.getElementById('close3').addEventListener('click', (e)=> {
                        //隐藏弹窗
                        e.stopPropagation();
                        document.getElementById('popup3').style.display = 'none'
                    })
                }
                delDiv.append(editBtn);

                //3s后还原
                setTimeout(function () {
                    delDiv.remove()
                },3000);
            }
            bookDiv.appendChild(book);
            bookContent.append(bookDiv)
        }
    }
}
//获取groupId合集(递归)：[groupId1 groupId2 ...]
function getDirs(children,dirList){
    if (children==null){
        return
    }
    for (let z = 0; z < children.length; z++) {
        dirList.push(children[z].groupId)
        getDirs(children[z].children,dirList)
    }
}
function renameBook(type,name,bookData,sortData,bookContent,shelfData){
    //获取目的文件名
    let newFileName=name+type

    // 文件重命名
    let newPath=bookData.bookPath.replace(bookData.fileName,newFileName)
    try {
        fs.renameSync(bookData.bookPath, newPath);
        console.log('文件重命名成功！');
    } catch (err) {
        console.error('文件重命名失败：', err);
    }

    // 构造笔记
    let pointDataArr = JSON.parse(fs.readFileSync(pointPath, 'utf8'));
    let pointData
    let flag = false
    for (let i = 0; i < pointDataArr.length; i++) {
        if (pointDataArr[i].fileName===bookData.fileName){
            pointData = pointDataArr[i]
            flag = true
            break
        }
    }
    if (flag){
        let newPointData=pointData
        newPointData.fileName=newFileName
        // 替换笔记
        pointDataArr.splice(pointDataArr.indexOf(pointData), 1,newPointData);
        // 保存到本地json
        fs.writeFileSync(pointPath, JSON.stringify(pointDataArr,null, '\t'));
    }

    // 构造阅读记录
    let newBookData = bookData
    let tmp=newFileName.substring(0,newFileName.lastIndexOf("."))
    let reg=/(((作者[:：])|(by)|(BY)|—|-|》|~|～).*)|《| /g
    newBookData.fileName= newFileName
    newBookData.bookName= tmp.replace(reg,'')
    newBookData.bookPath= newPath
    // 替换历史记录
    sortData.splice(sortData.indexOf(bookData), 1,newBookData);
    // 保存到本地json
    fs.writeFileSync(readPath, JSON.stringify(sortData,null, '\t'));

    // // 替换页面
    // book.innerHTML = newBookData.bookName
    // click事件太多了，懒得改，刷新书架得了
    getShelfBooks(bookContent,shelfData)

    //隐藏弹窗
    document.getElementById('popup3').style.display = 'none'
}

// 获取小说内容/阅读进度/目录，并渲染
// 本操作会先更新readData顺序、再初始化右击菜单、最后获取文件
function getStory(path) {
    //停止自动加载网络小说定时器
    clearInterval(getNextInterval);
    getNextInterval=null;

    // 排序
    //获取阅读记录
    let readData = JSON.parse(fs.readFileSync(readPath, 'utf8'));
    //按时间倒叙
    let sortData = sortJsonArr(readData, "desc", "readTime");
    //保存到本地json
    fs.writeFileSync(readPath, JSON.stringify(sortData,null, '\t'));

    //初始化书架菜单
    initShelfMenu()

    if (path===""){
        FileName = sortData[0].fileName
        BookPath = sortData[0].bookPath
        path=sortData[0].bookPath
    }

    // 获取该书的弹幕
    let pointData = JSON.parse(fs.readFileSync(pointPath, 'utf8'));
    for (let i = 0; i < pointData.length; i++) {
        if (pointData[i].fileName===FileName){
            duangData=pointData[i].duang;
            break
        }
    }

    let options = {
        url:"http://localhost:8708/upload",
        data: {
            "path": path,
            "userData":dataPath,
        },
        method: 'post',
        headers:{
            'Content-Type':'multipart/form-data'
        }
    }
    //发送请求，获取数据并渲染
    axios(options).then(
        (res)=>{
            if (res.data.code!==200){
                console.log(res.data.msg)
                ipcRenderer.invoke('logger', 'error', 'upload 渲染失败:'+res.data.msg)
                return
            }
            // 渲染
            let webview1 = this.document.querySelector('#content');
            webview1.innerHTML = res.data.body;

            // 重置目录规则
            dirId=0

            // 查找阅读进度
            for (let i = 0; i < sortData.length; i++) {
                if (sortData[i].fileName===FileName){
                    //字号
                    webview1.style.fontSize = sortData[i].fontSize;
                    //阅读滚动条
                    let scrollHeight2 = document.documentElement.scrollHeight || document.body.scrollHeight
                    document.documentElement.scrollTop  = 1.00*sortData[i].readHeight*scrollHeight2
                    // 获取目录规则
                    if (sortData[i].dirId!=null && sortData[i].dirId!==0){
                        dirId=sortData[i].dirId;
                    }

                    // 初始化init
                    // 后续可注释，仅用作配置文件升级使用
                    // 如不在乎性能也可以留着,移动文件后会在选择文件时 与匹配的文件名更新地址
                    // let tmp=FileName.substring(0,FileName.lastIndexOf("."))
                    // let reg=/(((作者[:：])|(by)|(BY)|—|-|》|~|～).*)|《| /g
                    // sortData[i].bookName=tmp.replace(reg,'')
                    // sortData[i].bookPath=BookPath
                    // fs.writeFileSync(readPath, JSON.stringify(sortData, null, '\t'));
                    break
                }
            }

            // 目录容器
            let dixDiv=document.getElementById('fix-dir')
            dixDiv.innerHTML=''

            //初始化章节目录
            let dirMenu=new Menu();
            //获取目录正则
            let dirRules = JSON.parse(fs.readFileSync(dirPath, 'utf8'));
            // map对相同规则名去重
            let repeatedMap = new Map();
            //遍历每种正则
            for (let i = 0; i < dirRules.length; i++) {
                // 若没添加过，则加入
                if(!(repeatedMap.hasOwnProperty(dirRules[i].dirName))){
                    repeatedMap[dirRules[i].dirName]=""

                    //获取对应子目录
                    let dirMap = new Map(Object.entries(res.data.chapters));
                    let chapters = dirMap.get(dirRules[i].dirName);
                    // 若为空则不显示
                    if (chapters.length===0){
                        continue;
                    }
                    // 若章节class未赋值，默认采用第一个不为空的正则目录
                    if (dirId===0) {
                        dirId = dirRules[i].id
                    }

                    //右侧菜单
                    let dirRule = document.createElement("div");
                    let id= 'chapter' + dirRules[i].id;
                    dirRule.id = id
                    dirRule.className='dir-rule'
                    dirRule.style.display='none';
                    //头部
                    let dirTop = document.createElement("div");
                    dirTop.innerText='目录 '+dirRules[i].dirName
                    dirTop.className='dir-head'
                    dirRule.appendChild(dirTop)
                    //目录
                    let dirgroup = document.createElement("div");
                    dirgroup.className='dir-group'
                    //遍历章节名
                    for (let j = 0; j < chapters.length; j++) {
                        let dir = document.createElement("p");
                        dir.id=id +'+'+ j
                        dir.innerText=chapters[j]
                        dir.title=chapters[j]
                        dir.onclick=function () {
                            let maodian = id +'-'+ j
                            document.getElementById(maodian).scrollIntoView();
                            // 若点击了则切换规则
                            dirId=dirRules[i].id
                        }
                        dirgroup.appendChild(dir)
                    }
                    dirRule.appendChild(dirgroup)
                    dixDiv.appendChild(dirRule)

                    // 添加到二级菜单
                    let tmpMenu = new MenuItem({
                        label: dirRules[i].dirName,
                        click: function () {
                            // 确保隐藏上一个目录弹窗
                            let lastId = 'chapter' +dirId
                            document.getElementById(lastId).style.display='none';

                            // 若点击了则切换规则
                            dirId = dirRules[i].id

                            // 显示右侧目录内容
                            let dir=document.getElementById(id)
                            dir.style.display='block';
                            if (document.body.style.backgroundColor==='rgba(255, 255, 255, 0)'||document.body.style.backgroundColor===''){
                                dir.style.backgroundColor='#ffffff';
                            }else {
                                dir.style.backgroundColor=document.body.style.backgroundColor;
                            }

                            // 目录转到对应章节
                            let chapters = document.getElementsByClassName(id);
                            let currentIndex = getCurrentChapter(chapters);
                            if (currentIndex>=0){
                                let maodian = id + '+' + currentIndex;
                                document.getElementById(maodian).scrollIntoView();
                            }

                            //失焦隐藏右侧目录
                            webview1.addEventListener('click', function() {
                                document.getElementById(id).style.display='none';
                            });
                        }
                    })
                    if(dirId===dirRules[i].id){
                        dirMenu.insert(0,tmpMenu)
                    }else {
                        dirMenu.append(tmpMenu)
                    }
                }
            }
            menuDir = new MenuItem(
                {
                    label:'目录',
                    type: 'submenu',
                    submenu: dirMenu,
                }
            )
            //清空主菜单
            mainMenu = new Menu();
            //添加到菜单组
            mainMenu.append(menuDir)
            mainMenu.append(menuColor)
            mainMenu.append(menuBg)
            mainMenu.append(menuScroll)
            mainMenu.append(menuShelf)
        }
    ).catch(
        (error)=>{
            console.log('getStory渲染失败:',error)
            ipcRenderer.invoke('logger', 'error', 'getStory渲染失败:'+error)
        }
    )
}
// 获取网络小说内容/阅读进度/目录
function getOnlineNovel(novelHistory) {
    //停止自动加载网络小说定时器
    clearInterval(getNextInterval);
    getNextInterval=null;

    // 排序
    //获取阅读记录
    let readData = JSON.parse(fs.readFileSync(readPath, 'utf8'));
    //按时间倒叙
    let sortData = sortJsonArr(readData, "desc", "readTime")
    //保存到本地json
    fs.writeFileSync(readPath, JSON.stringify(sortData,null, '\t'));

    //初始化书架菜单
    initShelfMenu()

    //获取源规则
    let source
    let searchRules = JSON.parse(fs.readFileSync(searchPath, 'utf8'));
    for (let i = 0; i < searchRules.length; i++) {
        if (novelHistory.searchId===searchRules[i].id){
            source=searchRules[i];
            break;
        }
    }

    let webview1 = document.getElementById('content');
    webview1.innerHTML=''

    //获取目录
    let options2 = {
        url:"http://localhost:8708/story/chapters",
        data: {
            "url": source.url+novelHistory.bookPath,
            "chapter":source.chapter,
        },
        method: 'post',
        headers:{
            'Content-Type':'multipart/form-data'
        }
    }
    //发送请求，获取数据并渲染
    axios(options2).then(
        (res2)=>{
            if (res2.data.code!==200){
                console.log(res2.data.msg)
                ipcRenderer.invoke('logger', 'error', 'story/chapters 渲染失败:'+res2.data.msg)
                return
            }
            onlineChapters=res2.data.body
            console.log(onlineChapters)
            // 目录规则设为-1
            dirId=-1
            let id ='chapter'+ dirId

            // 目录容器
            let dixDiv=document.getElementById('fix-dir')
            //右侧菜单
            let dirRule = document.createElement("div");
            dirRule.id = 'chapter-online';
            dirRule.className='dir-rule'
            dirRule.style.display='none';
            //头部
            let dirTop = document.createElement("div");
            dirTop.innerText='目录'
            dirTop.className='dir-head'
            dirRule.appendChild(dirTop)
            //目录
            let dirgroup = document.createElement("div");
            dirgroup.className='dir-group'

            //最先加载的章节
            let index = 0;

            // 遍历章节名
            for (let j = 0; j < onlineChapters.length; j++) {
                // 内容添加空的div
                let empty = document.createElement("div");
                empty.className=id
                let maodian = id +'-'+ j
                empty.id=maodian
                // 添加章节名&点击事件
                let chapterName = document.createElement("p");
                chapterName.id=maodian+'-get'
                chapterName.innerText=onlineChapters[j].name
                chapterName.style.cursor="pointer"
                chapterName.title = '点击加载本章内容';
                chapterName.onclick = function (){
                    if (j===onlineChapters.length-1){
                        getOneChapter(2,onlineChapters[j].name, onlineChapters[j].href, source, novelHistory.bookPath, j)
                    }else {
                        getOneChapter(2,onlineChapters[j].name, onlineChapters[j].href, source, onlineChapters[j+1].href, j)
                    }
                }
                empty.appendChild(chapterName)
                webview1.appendChild(empty)
                //目录
                let dir = document.createElement("p");
                dir.id = id +'+'+ j
                dir.innerText = onlineChapters[j].name
                dir.title = onlineChapters[j].name
                dir.onclick = function () {
                    // 如果为空就发请求获取内容
                    if (document.getElementById(maodian).innerText===onlineChapters[j].name){
                        // 已经是最后一章
                        if (j===onlineChapters.length-1){
                            getOneChapter(0,onlineChapters[j].name, onlineChapters[j].href, source, novelHistory.bookPath,j)
                        }else {
                            getOneChapter(0,onlineChapters[j].name, onlineChapters[j].href, source, onlineChapters[j+1].href,j)
                        }
                    }
                    document.getElementById(maodian).scrollIntoView();
                }
                dirgroup.appendChild(dir)
                //首先应加载的章节
                if (novelHistory.readUrl===onlineChapters[j].href){
                    index=j
                }
            }
            dirRule.appendChild(dirgroup)
            dixDiv.appendChild(dirRule)

            menuDir = new MenuItem({
                label:'目录',
                click: function () {
                    // 显示右侧目录内容
                    let dir=document.getElementById('chapter-online')
                    dir.style.display='block';
                    if (document.body.style.backgroundColor==='rgba(255, 255, 255, 0)'||document.body.style.backgroundColor===''){
                        dir.style.backgroundColor='#ffffff';
                    }else {
                        dir.style.backgroundColor=document.body.style.backgroundColor;
                    }

                    // 目录导航栏转到对应章节
                    let chapters = document.getElementsByClassName(id);
                    let currentIndex = getCurrentChapter(chapters);
                    if (currentIndex<0){
                        document.documentElement.scrollTop=0
                    }else {
                        let dirMaodian = id +'+'+ currentIndex;
                        document.getElementById(dirMaodian).scrollIntoView();
                    }

                    //失焦隐藏右侧目录
                    webview1.addEventListener('click', function() {
                        document.getElementById('chapter-online').style.display='none';
                    });
                }
            });

            //清空主菜单
            mainMenu = new Menu();
            //添加到菜单组
            mainMenu.append(menuDir)
            mainMenu.append(menuColor)
            mainMenu.append(menuBg)
            mainMenu.append(menuScroll)
            mainMenu.append(menuShelf)

            //加载初始章
            if (index===onlineChapters.length-1){
                getOneChapter(novelHistory.readHeight,onlineChapters[index].name, novelHistory.readUrl, source, novelHistory.bookPath,index)
            }else {
                getOneChapter(novelHistory.readHeight,onlineChapters[index].name, novelHistory.readUrl, source, onlineChapters[index+1].href,index)
            }
        }).catch(
        (error)=>{
            console.log('getOnlineNovel 获取网络小说章节失败:',error)
            ipcRenderer.invoke('logger', 'error', 'getOnlineNovel 获取网络小说章节失败:'+error)
        }
    )

    // 等待10s后开启检测：自动加载下一章
    setTimeout(getNextInterval = setInterval(getNextChapter,10000),10000);

}
// 获取并渲染章节，readHeight表示跳转历史进度: [0,1] 2表示不跳转
function getOneChapter(readHeight,chapterName,start,source,end,index) {
    // 设置按钮不可再点击
    let id ='chapter' + dirId
    let chapterNameP = document.getElementById( id+ '-'+ index +'-get');
    chapterNameP.disabled = true;
    chapterNameP.title = '加载中...';
    let chapterNameDir = document.getElementById( id+ '+'+ index);
    chapterNameDir.disabled = true;
    // 校验传参
    if (start==="" || end ===""){
        console.log("非法传参")
        return
    }
    //判断当前章节的下一章节的链接
    let options = {
        url:"http://localhost:8708/story/page",
        data: {
            "url":source.url,
            "start": start,
            "end": end,
            "body":source.body,
            "last":source.last,
            "next":source.next,
        },
        method: 'post',
            headers:{
            'Content-Type':'multipart/form-data'
        }
    }
    //发送请求，获取数据并渲染
    axios(options).then(
        (res)=>{
            //恢复点击
            chapterNameP.disabled = false;
            chapterNameP.title = '点击加载本章内容';
            chapterNameDir.disabled = false;

            console.log("本章内容:",res.data)

            let chapterDiv = document.getElementById(id +'-'+ index);
            // 渲染
            if (res.data.code===200){
                chapterDiv.innerHTML='<p>'+chapterName+'</p>'+res.data.body;
            }else {
                console.log(res.data.msg)
                ipcRenderer.invoke('logger', 'error', 'story/page 渲染失败:'+res.data.msg)
                let chapterBody = document.createElement("p");
                chapterBody.innerHTML=res.data.body
                chapterDiv.appendChild(chapterBody);
            }
            if (readHeight!==2){
                chapterDiv.scrollIntoView();
                let scrollNow = document.documentElement.scrollTop;
                let scrollAdd = 1.00 * readHeight * chapterDiv.getBoundingClientRect().height;
                document.documentElement.scrollTop = scrollNow - scrollAdd;
            }
        }
    ).catch(
        (error)=>{
            console.log('getOneChapter 获取章节内容失败:',error)
            ipcRenderer.invoke('logger', 'error', 'getOneChapter 获取章节内容失败:'+error)
        }
    )
}
// 每10s检测一次是否要触发
function getNextChapter() {
    //获取当前章
    // 获取所有章节
    let chapters = document.getElementsByClassName('chapter'+dirId);
    // 二分法查找下一spanId chapter[id]-[index] 的索引index
    let currentIndex = getCurrentChapter(chapters);
    //若不是最后一章
    if(++currentIndex!==chapters.length){
        //判断是否加载了（点击文章事件是否还在）
        if(document.getElementById('chapter'+dirId+'-'+currentIndex+'-get')!=null){
            //停止定时器
            clearInterval(getNextInterval);
            getNextInterval=null;
            //获取下一章
            document.getElementById('chapter'+dirId+'-'+currentIndex+'-get').click();
            //启动定时器
            getNextInterval = setInterval(getNextChapter, 10000);
        }
    }
}

//停止翻页&保存浏览记录
ipcRenderer.on('save', (e, args) => {
    // 停止翻页
    clearInterval(ref);
    ref=null
    // 保存记录
    if (FileName!==""){
        save()
    }
    // 保存设置
    saveConfig()
})
// 保存阅读记录 不排序
function save() {
    // 保存设置
    saveConfig()

    //滚动条百分比距离（是否有开始阅读，还是只单纯打开了一个新文件）
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    if(scrollTop!==0) {
        let element = document.getElementById("content");
        //字号
        let style = window.getComputedStyle(element, null).getPropertyValue('font-size');
        let fontSize = parseFloat(style) + 'px';
        //修改时间
        let readTime = new Date().getTime()
        //读取本地
        let readData = JSON.parse(fs.readFileSync(readPath, 'utf8'));

        //更新对应文件名的旧记录
        let changeFlag = false;
        for (let i = 0; i < readData.length; i++) {
            //遍历找到对应名 更新同名文件
            if (readData[i].fileName === FileName) {
                changeFlag = true
                //字号
                readData[i].fontSize = fontSize

                //网络/本地
                if (readData[i].online){
                    // 取当前章节
                    let chapters = document.getElementsByClassName('chapter'+dirId);
                    let currentIndex = getCurrentChapter(chapters);

                    // 起始url
                    readData[i].readUrl = onlineChapters[currentIndex].href
                    // 阅读滚动条
                    let chapterDiv = document.getElementById('chapter'+dirId+'-'+currentIndex).getBoundingClientRect();
                    //本章整体高度
                    let chapterHeight = chapterDiv.height
                    //本章头部距离可视窗高度
                    let chapterTop = chapterDiv.top;
                    //保存阅读比例
                    readData[i].readHeight = (chapterTop * 1.00) / chapterHeight

                    // 最后阅读时间
                    readData[i].readTime = readTime
                }else {
                    //阅读滚动条
                    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
                    readData[i].readHeight = (scrollTop * 1.00) / scrollHeight
                    //如果有同名文件，以最后一次打开的路径为准
                    readData[i].bookPath=BookPath
                    //最后阅读时间
                    readData[i].readTime = readTime
                }
                // 存放到阅读记录中，每次读取该书都默认该正则（仅能设定一个id）
                readData[i].dirId = dirId
                break
            }
        }
        // 没查到记录，则添加新文件记录
        if (!changeFlag) {
            // 书名
            let tmp=FileName.substring(0,FileName.lastIndexOf("."))
            let reg=/(((作者[:：])|(by)|(BY)|—|-|》|~|～).*)|《| /g
            //阅读滚动条
            let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
            let newFile = {
                fileName: FileName,
                fontSize: fontSize,
                readHeight: (scrollTop * 1.00) / scrollHeight,
                readTime: readTime,
                dirId: dirId,
                groupId: 0,
                bookName: tmp.replace(reg,''),
                bookPath: BookPath,
            }
            readData.push(newFile)
        }

        //保存在本地json
        fs.writeFileSync(readPath, JSON.stringify(readData, null, '\t'));
    }
}

//回到顶部
ipcRenderer.on('to-top', (e, args) => {
    if (FileName!==""){
        save()
    }
    document.documentElement.scrollTop=0
})

/**
 *
 * @param resourceData 源数据
 * @param order desc-降序,asc-升序
 * @param keyName 排序字段
 */
function sortJsonArr(resourceData, order, keyName) {
    resourceData.sort(function (a, b) {
        if (order == "asc") {
            return a[keyName] > b[keyName] ? 1 : -1
        } else {
            return a[keyName] < b[keyName] ? 1 : -1
        }
    });
    return resourceData;
}

// 翻页
ipcRenderer.on('scroll', (e, args) => {
    if (ref) {
        clearInterval(ref);
        ref=null
    }else{
        let initHeight = document.documentElement.scrollTop;
        ref = setInterval(()=>{
            window.scroll(0 ,++initHeight)
        },scrollSpeed);

    }
})
// 滚动时翻页（防止重影
document.addEventListener('mousewheel',function(){
    if (ref) {
        //关闭旧定时器
        clearInterval(ref);
        ref=null
        //1000ms/1s后重启
        setTimeout(function () {
            let initHeight = document.documentElement.scrollTop;
            ref = setInterval(()=>{
                window.scroll(0 ,++initHeight)
            },scrollSpeed);
        },1000);
    }
});

// 观后感
ipcRenderer.on('share-points', (e, args) => {
    if (FileName===""){
        return
    }
    // 读取本地
    let pointData = JSON.parse(fs.readFileSync(pointPath, 'utf8'));

    //获取文名
    let content = FileName.substring(0,FileName.lastIndexOf("."));
    // 网络小说
    if (content===""){
        content=FileName
    }
    // 获取对应文点评，有内容就直接替换
    let index = pointData.length;
    for (let i = 0; i < pointData.length; i++) {
        if (pointData[i].fileName===FileName){
            index=i;
            content=pointData[i].contentPoints.replace(/\n/g,"<br/>");
            break
        }
    }

    // 显示内容
    let points =  document.querySelector('#points');
    points.innerHTML = content;
    // 显示弹窗
    if (document.body.style.backgroundColor==='rgba(255, 255, 255, 0)'||document.body.style.backgroundColor===''){
        document.getElementById('popup-content').style.backgroundColor='#ffffff';
    }else {
        document.getElementById('popup-content').style.backgroundColor=document.body.style.backgroundColor;
    }
    document.getElementById('popup').style.display = 'block';

    // 监听输入框，如果有更改则检索修改/追加
    points.addEventListener('input', function (){
        // 获取内容
        let stringData=points.innerText
        // 获取时间
        let now = new Date().toLocaleDateString();

        // 更新
        if (index!==pointData.length){
            pointData[index].fileName = FileName;
            pointData[index].contentPoints= stringData;
            pointData[index].readTime=now;
        }else {
            //没查到记录，则添加新文件记录
            let newFile = {
                fileName: FileName,
                contentPoints: stringData,
                readTime: now
            }
            pointData.push(newFile)
        }

        //保存在本地json
        fs.writeFileSync(pointPath, JSON.stringify(pointData,null, '\t'));
    })

    // 监听关闭按钮
    document.getElementById('close').addEventListener('click', (e)=> {
        //隐藏弹窗
        e.stopPropagation();
        document.getElementById('popup').style.display = 'none'
    })
})

// 下一章
ipcRenderer.on('next-chapter', (e, args) =>{
    // 获取所有章节
    let chapters = document.getElementsByClassName('chapter'+dirId);
    // 二分法查找下一spanId chapter[id]-[index] 的索引index
    let currentIndex = getCurrentChapter(chapters);
    // 滚动到下一章
    if (++currentIndex===chapters.length){
        document.documentElement.scrollTop=document.documentElement.scrollHeight || document.body.scrollHeight
    }else {
        let spanId='chapter'+dirId+'-'+currentIndex;
        document.getElementById(spanId).scrollIntoView();
    }
})
// 上一章
ipcRenderer.on('last-chapter', (e, args) =>{
    // 获取所有章节
    let chapters = document.getElementsByClassName('chapter'+dirId);
    // 二分法查找下一spanId chapter[id]-[index] 的索引index
    let currentIndex = getCurrentChapter(chapters);
    // 滚动到上一章
    if (--currentIndex<0){
        document.documentElement.scrollTop=0
    }else {
        let spanId='chapter'+dirId+'-'+currentIndex;
        document.getElementById(spanId).scrollIntoView();
    }
})
// 二分法查找下一章spanId chapter[id]-[index] 的索引index
function getCurrentChapter(chapters) {
    // 获取距顶部高度
    let height=document.documentElement.scrollTop;
    // 二分法边界
    let left = 0; // 左边界索引
    let right = chapters.length - 1; // 右边界索引
    while (left <= right) {
        // 计算中间位置索引
        let mid = Math.floor((left + right) / 2);
        // 获取该元素距顶部高度，有误差
        let midHeight= document.getElementById(chapters[mid].id).offsetTop-5;
        // 如果高度相等
        if (midHeight === height) {
            let midHeight= document.getElementById(chapters[mid].id).offsetTop;
            return mid;//当前章节索引
        } else if (height < midHeight) {
            right = mid - 1; // 如果目标值小于当前中间位置的值，将右边界设为中间位置-1
        } else {
            left = mid + 1; // 如果目标值大于当前中间位置的值，将左边界设为中间位置+1
        }
    }
    // 左大于右
    // -1为非正文，0为第一章…
    return right;
}

//刷新
ipcRenderer.on('refresh', (e, args) => {
    if (FileName !== "") {
        // 停止滚动
        clearInterval(ref);
        ref = null
        // 保存进度
        save()
    }else {
        // 保存配置
        saveConfig()
    }
    // 返回顶部
    document.documentElement.scrollTop=0
    // 刷新页面
    location.reload();
})

//不保存直接退出，返回书架
ipcRenderer.on('quit', (e, args) => {
    // 停止滚动
    clearInterval(ref);
    ref = null
    // 保存配置
    saveConfig()
    // 返回顶部
    document.documentElement.scrollTop=0
    // 刷新页面
    location.reload();
})

//阅读百分比统计
ipcRenderer.on('read-schedule', (e, args) => {
    //阅读滚动条
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
    let counter = ((scrollTop * 1.00) / scrollHeight)*100
    let wordCount = document.getElementById('read-schedule');
    wordCount.textContent = counter.toFixed(2)+'%';
    //3s后还原
    setTimeout(function () {
        wordCount.textContent=''
    },3000);
})

// 添加弹幕
ipcRenderer.on('add-danmu', (e, args) => {
    if (FileName===""){
        return
    }
    // 显示弹窗
    let duangElement=document.getElementById('duang');
    if (document.body.style.backgroundColor==='rgba(255, 255, 255, 0)'||document.body.style.backgroundColor===''){
        duangElement.style.backgroundColor='#ffffff';
    }else {
        duangElement.style.backgroundColor=document.body.style.backgroundColor;
    }
    duangElement.style.display = 'block';

    // 输入光标
    let duangInputEle=document.getElementById('duang-input');
    duangInputEle.focus();

    // 回车则保存弹幕
    duangInputEle.addEventListener('keyup', function(event) {
        event.preventDefault();
        // 检查按键是否是回车键
        if (event.keyCode === 13) {
            let content=this.value
            // 清除内容
            duangInputEle.value=null
            // 隐藏弹窗
            document.getElementById('duang').style.display = 'none';
            if (content===""){
                return
            }
            // 获取高度
            let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
            let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
            let clientHeight = document.documentElement.clientHeight || window.innerHeight
            // 构造新弹幕
            let newDanmu = {
                height: (scrollTop+clientHeight/2) * 1.00 / scrollHeight,
                content: content,
                time:new Date().toLocaleDateString() + ' ' + new Date().toLocaleTimeString()
            }
            // 保存到json
            let pointData = JSON.parse(fs.readFileSync(pointPath, 'utf8'));
            let notFindFlag = true
            for (let i = 0; i < pointData.length; i++) {
                if (pointData[i].fileName===FileName){
                    if (pointData[i].duang){
                        // 添加弹幕
                        pointData[i].duang.push(newDanmu)
                    }else {
                        pointData[i].duang=[newDanmu]
                    }
                    // 排序
                    pointData[i].duang.sort((a, b) => a.height - b.height);
                    // 更新页面缓存
                    duangData=pointData[i].duang
                    notFindFlag=false
                    break
                }
            }
            if (notFindFlag){
                let newPoint = {
                    fileName: FileName,
                    contentPoints: FileName.substring(0,FileName.lastIndexOf(".")),
                    readTime: new Date().toLocaleDateString(),
                    duang:[newDanmu]
                }
                pointData.push(newPoint)
                // 更新页面缓存
                duangData=[newDanmu]
            }
            //保存在本地json
            fs.writeFileSync(pointPath, JSON.stringify(pointData,null, '\t'));
            // 展示
            showBarrage(newDanmu,scrollTop+clientHeight/2-15);
        }
    })
})
// 监听滚动事件：展示弹幕
window.addEventListener('scroll', throttle(() => {
    if (FileName==="" || !duangData){
        return
    }
    let scrollTop = document.documentElement.scrollTop || document.body.scrollTop
    let scrollHeight = document.documentElement.scrollHeight || document.body.scrollHeight
    let clientHeight = document.documentElement.clientHeight || window.innerHeight

    // 计算当前可视窗口的顶部和底部位置对应的相对高度百分比
    let viewTop = scrollTop * 1.00 / scrollHeight;
    let viewBottom = (scrollTop + clientHeight) * 1.00/ scrollHeight;

    // 遍历JSON数据中的评论
    // 维护上次高度：防止弹幕重叠
    let add=0
    let last=0
    duangData.forEach(comment => {
        // 检查弹幕是否在可视窗口内 且不重复
        if (comment.height > viewTop && comment.height < viewBottom && !comment.showing) {
            let now=1.00*comment.height*scrollHeight
            if (now-last<15){
                add++
                now+=15*add
                last=now
            }else {
                add=0
                last=now
            }
            // 显示弹幕
            showBarrage(comment,now);
        }
    });
},500));
// 限制触发间隔
function throttle(func, limit) {
    let inThrottle;
    return function() {
        const args = arguments;
        const context = this;
        if (!inThrottle) {
            func.apply(context, args);
            inThrottle = true;
            setTimeout(() => inThrottle = false, limit);
        }
    }
}
// 弹幕显示函数
function showBarrage(comment,height) {
    comment.showing=true

    let barrage = document.createElement('div');
    barrage.classList.add('barrage');
    barrage.textContent = comment.content;
    // barrage.title=comment.time;
    document.getElementById('barrage-container').appendChild(barrage);

    barrage.style.top  = `${height}px`;

    // 弹幕动画
    let animation = barrage.animate([
        { transform: 'translateX(100%)' },
        { transform: 'translateX(-100%)' }
    ], {
        duration: (comment.content.length * 200) + 3000, // 根据内容长度调整动画时间
        easing: 'linear',
        fill: 'forwards'
    });

    // 鼠标悬停事件
    barrage.addEventListener('mouseenter', () => {
        animation.pause();
    });

    // 鼠标移开事件
    barrage.addEventListener('mouseleave', () => {
        animation.play();
    });

    // 动画完成后移除弹幕
    animation.finished.then(() => {
        barrage.remove();
        comment.showing = false;
    });
}