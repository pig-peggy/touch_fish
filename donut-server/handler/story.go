package handler

import (
	"donut-server/logic"
	"github.com/PuerkitoBio/goquery"
	"github.com/gin-gonic/gin"
	"net/http"
)

// GetStoryHandleFunc 读取小说
// @Summary 读取小说
// @Description 上传文件
// @Tags 上传文件
// @Param path formData string true "文件绝对路径"
// @Param userData formData string true "userData目录"
// @Router /upload [POST]
func GetStoryHandleFunc(c *gin.Context) {
	// 绝对路径
	path, _ := c.GetPostForm("path")
	userData, _ := c.GetPostForm("userData")
	//读取文件
	body, chapters, err := logic.GetLocalStory(path, userData)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  err,
		})
	}

	//返回数据
	c.JSON(http.StatusOK, gin.H{
		"code":     http.StatusOK,
		"body":     body,
		"chapters": chapters,
	})
}

//=============================================================================================

// IndexStoryHandleFunc 小说章节目录
// @Summary 小说章节目录
// @Description 小说章节目录
// @Tags 查找小说
// @Param url formData string true "小说目录完整url"
// @Param chapter formData string true "章节列表标签"
// @Router /story/chapters [POST]
func IndexStoryHandleFunc(c *gin.Context) {
	url, _ := c.GetPostForm("url")
	chapter, _ := c.GetPostForm("chapter")
	type Chapter struct {
		Name string `json:"name"`
		Href string `json:"href"`
	}
	list := make([]Chapter, 0)

	//doc, _ := goquery.NewDocument(url)
	doc, err := logic.GetHtmlBody(url)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  err,
		})
	}

	doc.Find(chapter).Find("a").Each(func(i int, selection *goquery.Selection) {
		href, _ := selection.Attr("href")
		name := selection.Text()
		list = append(list, Chapter{
			Name: name,
			Href: href,
		})
	})

	//返回数据
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"body": list,
	})
}

// PageStoryHandleFunc 当前页
// @Summary 当前页
// @Description 当前页
// @Tags 查找小说
// @Param url formData string true "网站首页"
// @Param start formData string true "当前章链接"
// @Param end formData string true "截至章链接"
// @Param body formData string true "内容标签"
// @Param last formData string true "上一页"
// @Param next formData string true "下一页"
// @Router /story/page [POST]
func PageStoryHandleFunc(c *gin.Context) {
	url, _ := c.GetPostForm("url")
	start, _ := c.GetPostForm("start")
	end, _ := c.GetPostForm("end")
	body, _ := c.GetPostForm("body")
	last, _ := c.GetPostForm("last")
	next, _ := c.GetPostForm("next")
	code := http.StatusOK
	//读取文件
	text, lastUrl, nextUrl, err := logic.GoSearch(url+start, body, last, next)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  err,
		})
	}
	for i := 0; i < 15; i++ {
		//最后一章
		if end == nextUrl {
			break
		}
		//访问太频繁了，就直接停止
		if nextUrl == "" {
			text = "<br><br>访问太频繁了，返回数据异常<br><br>"
			code = http.StatusNotFound
			break
		}
		var text0 string
		text0, lastUrl, nextUrl, err = logic.GoSearch(url+nextUrl, body, last, next)
		if err != nil {
			c.JSON(http.StatusOK, gin.H{
				"code": http.StatusInternalServerError,
				"msg":  err,
			})
		}
		text = text + "<br><br>" + text0
	}
	//返回数据
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"body":    text,
		"lastUrl": lastUrl,
		"nextUrl": nextUrl,
	})
}
