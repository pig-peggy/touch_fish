package handler

import (
	"donut-server/logic"
	"github.com/gin-gonic/gin"
	"net/http"
)

//全局搜索-最新发布：	https://www.douban.com/group/search?cat=1013&q=天堂放逐者
//		-相关度：		https://www.douban.com/group/search?cat=1013&q=天堂放逐者&sort=relevance
//小组搜索-无关键字：	https://www.douban.com/group/693205/discussion?start=0&type=new
//		-最新发布：	https://www.douban.com/group/search&cat=1013?group=661193&q=天堂放逐者&sort=time
//		-相关度：		https://www.douban.com/group/search?cat=1013&group=661193&q=天堂放逐者&sort=relevance

// GetListHandleFunc 获取小组话题列表
// @Summary 获取小组话题
// @Description 获取小组话题
// @Tags 查找豆瓣
// @Param url formData string true "讨论列表url"
// @Param userData formData string true "userData目录"
// @Router /dou/list [POST]
func GetListHandleFunc(c *gin.Context) {
	url, _ := c.GetPostForm("url")
	userData, _ := c.GetPostForm("userData")
	list, err := logic.GetGroupTopics(userData, url)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  err,
		})
	}
	//返回数据
	c.JSON(http.StatusOK, gin.H{
		"code": http.StatusOK,
		"body": list,
	})
}

// GetTopicHandleFunc 获取话题内容
// @Summary 获取话题内容
// @Description 获取话题内容
// @Tags 查找豆瓣
// @Param url formData string true "讨论话题页"
// @Param userData formData string true "userData目录"
// @Router /dou/topic [POST]
func GetTopicHandleFunc(c *gin.Context) {
	url, _ := c.GetPostForm("url")
	userData, _ := c.GetPostForm("userData")
	list, group, err := logic.GetOneTopic(userData, url)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"code": http.StatusInternalServerError,
			"msg":  err,
		})
	}
	//返回数据
	c.JSON(http.StatusOK, gin.H{
		"code":  http.StatusOK,
		"body":  list,
		"group": group,
	})
}
