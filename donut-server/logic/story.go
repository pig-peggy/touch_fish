package logic

import (
	"bufio"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"golang.org/x/net/html/charset"
	"golang.org/x/text/encoding"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"html"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"
)

// GetLocalStory 获取本地小说
func GetLocalStory(path, userData string) (string, map[string][]string, error) {
	// 读取目录规则
	//从文件中读取JSON数据
	//⚠️注意手写json文件时转义特殊字符
	dataDir, err := ioutil.ReadFile(userData + "dirRules.json")
	if err != nil {
		return "", nil, err
	}
	type DirRule struct {
		Id        int    `json:"id"`
		DirName   string `json:"dirName"`
		DirSource string `json:"dirSource"`
	}
	dirRules := make([]DirRule, 0)
	// 解析JSON数据到结果变量中
	err = json.Unmarshal(dataDir, &dirRules)
	if err != nil {
		return "", nil, err
	}
	//初始化目录
	//目录map
	dirMap := map[string][]string{}
	for _, rule := range dirRules {
		// 很重要！！保证map值为空时前端也能找到空值！
		dirMap[rule.DirName] = make([]string, 0)
	}

	//打开文件
	file, err := os.Open(path)
	if err != nil {
		return "", nil, err
	}
	defer file.Close()

	//判断编码类型
	e, err := determineEncode(path)
	if err != nil {
		return "", nil, err
	}
	//按照文件编码格式构造读取方式
	reader := transform.NewReader(file, e.NewDecoder())
	//读取文件内容
	content := bufio.NewReader(reader)

	//按行内容拼接字符串
	var b strings.Builder
	//兜底目录，200行切割成一章
	nowChapter := 0
	nowLines := 0
	for {
		//内容拆分成行
		hang, _, err := content.ReadLine()
		if err != nil && err != io.EOF {
			return "", nil, err
		}
		b.WriteString("<p>")
		nowLines++
		//目录
		for _, rule := range dirRules {
			//目录正则
			pattern := rule.DirSource
			reg := regexp.MustCompile(pattern)
			c := reg.FindString(string(hang))
			if c != "" {
				dirMap[rule.DirName] = append(dirMap[rule.DirName], c)
				//正文
				b.WriteString(fmt.Sprintf("<span id='chapter%v-%v' class='chapter%v'></span>", rule.Id, len(dirMap[rule.DirName])-1, rule.Id))
			}
		}
		if nowLines == 1 {
			nowChapter++
			dirMap["兜底"] = append(dirMap["兜底"], fmt.Sprintf("第%v章", nowChapter))
			b.WriteString(fmt.Sprintf("<span id='chapter-1-%v' class='chapter-1'></span>", len(dirMap["兜底"])-1))
		}
		if nowLines == 200 {
			nowLines = 0
		}
		// 转义特殊字符
		b.WriteString(html.EscapeString(string(hang)))
		b.WriteString("</p>")

		//最后一行结束读取
		if err == io.EOF {
			break
		}
	}

	return b.String(), dirMap, nil
}

func determineEncode(str string) (encoding.Encoding, error) {
	//打开文件
	file, err := os.Open(str)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	//读取文件
	bytes, err := bufio.NewReader(file).Peek(1024)
	if err != nil && err != io.EOF {
		return nil, err
	}
	// 读取ANSI类型的文件，会判断name为"windows-1252"，用这种编码解析会乱码，应用gbk
	determineEncoding, name, _ := charset.DetermineEncoding(bytes, "text/plain")
	if name == "windows-1252" {
		return simplifiedchinese.GBK, nil
	}
	return determineEncoding, nil
}

func GetHtmlBody(url string) (*goquery.Document, error) {
	//创建客户端对象，发送请求
	var client = http.Client{}
	//发送请求
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)")
	// 发送请求
	resp, err := client.Do(req)
	// 处理异常
	if err != nil {
		return nil, err
	}
	// 关闭流
	defer resp.Body.Close()
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}
	return doc, nil
}

func GoSearch(url, body, last, next string) (text, lastUrl, nextUrl string, err error) {
	//doc, _ := goquery.NewDocument(url)
	doc, err := GetHtmlBody(url)
	if err != nil {
		return "", "", "", err
	}

	doc.Find(body).Each(func(i int, selection *goquery.Selection) {
		text, _ = selection.Html()
	})
	doc.Find("a").Each(func(i int, s *goquery.Selection) {
		txt := s.Text()

		lasts := strings.Split(last, ",")
		for _, l := range lasts {
			if txt == l {
				href, exists := s.Attr("href")
				if exists {
					lastUrl = href
					break
				}
			}
		}

		nexts := strings.Split(next, ",")
		for _, n := range nexts {
			if txt == n {
				href, exists := s.Attr("href")
				if exists {
					nextUrl = href
					break
				}
			}
		}
	})
	return
}
