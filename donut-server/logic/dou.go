package logic

import (
	"encoding/json"
	"github.com/PuerkitoBio/goquery"
	"html"
	"io/ioutil"
	"net/http"
	"strings"
)

// GetGroupTopics 获取小组讨论列表
func GetGroupTopics(userData, url string) (string, error) {
	//获取doc
	doc, err := getDoc(userData, url)
	if err != nil {
		return "", err
	}

	//移除 js、css、发言、跳转
	doc.Find("script").Remove()
	doc.Find("link").Remove()
	doc.Find("#group-new-topic-bar, .srh-tabs, .srh-filter, .group-search").Remove()
	doc.Find("td[nowrap]").Find("a").RemoveAttr("href")
	// 替换文章href为onclick
	doc.Find(".olt").Find("tr").Find(".title, .td-subject").Find("a").Each(func(i int, selection *goquery.Selection) {
		href, _ := selection.Attr("href")
		title, _ := selection.Attr("title")
		title = strings.ReplaceAll(title, "\n", "")
		title = strings.ReplaceAll(title, " ", "")
		selection.SetAttr("onclick", `getDouTopic('`+html.EscapeString(title)+"','"+href+`')`)
		selection.RemoveAttr("href")
	})
	// 替换翻页href为onclick
	doc.Find(".paginator").Find("a").Each(func(i int, selection *goquery.Selection) {
		href, _ := selection.Attr("href")
		selection.SetAttr("onclick", `getDouList('`+href+`')`)
		selection.RemoveAttr("href")
	})
	doc.Find(".article").Find("a").RemoveAttr("href")
	res, err := doc.Find(".article").Html()
	return res, err
}

// GetOneTopic 获取讨论内容及回帖
func GetOneTopic(userData, url string) (string, string, error) /**Topic*/ {
	//获取doc
	doc, err := getDoc(userData, url)
	if err != nil {
		return "", "", err
	}
	// 小组名
	group := doc.Find("#g-side-info").Find(".title").Find("a").Text()
	if group == "" {
		group = doc.Find("#g-side-info-member").Find(".title").Find("a").Text()
	}
	//移除图片/视频
	doc.Find("img, .user-face, .image-wrapper, .comment-photos, iframe").Remove()
	doc.Find("script, link").Remove()
	doc.Find(".dialog_join_group, .operation-more, .lnk-reply, .txd, #sep, h2, .pl3").Remove()
	doc.Find(".reply-quote").Find(".short").Remove()
	// h标签
	doc.Find("h1, h2, h3, h4, h5, h6").Each(func(i int, s *goquery.Selection) {
		s.ReplaceWithHtml("<p>" + s.Text() + "</p>")
	})
	// 替换翻页href为onclick
	doc.Find(".paginator").Find("a").Each(func(i int, selection *goquery.Selection) {
		href, _ := selection.Attr("href")
		title, _ := selection.Attr("title")
		title = strings.ReplaceAll(title, "\n", "")
		title = strings.ReplaceAll(title, " ", "")
		selection.SetAttr("onclick", `getDouTopic('`+html.EscapeString(title)+"','"+href+`')`)
		selection.RemoveAttr("href")
	})
	// 移除a标签
	doc.Find(".article").Find("a").RemoveAttr("href")
	// 帖子内容
	res, _ := doc.Find(".article").Html()
	return res, group, nil
}

func getDoc(userData string, url string) (doc *goquery.Document, err error) {
	// 获取豆瓣Cookie
	dataDou, err := ioutil.ReadFile(userData + "userConfig.json")
	if err != nil {
		return nil, err
	}
	type Config struct {
		DouCookie string `json:"douCookie"`
	}
	config := Config{}
	// 解析JSON数据到结果变量中
	err = json.Unmarshal(dataDou, &config)
	if err != nil {
		return nil, err
	}

	//创建客户端对象，发送请求
	var client = http.Client{}
	//发送请求
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Accept", "ext/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
	req.Header.Set("Accept-Language", "zh-CN,zh;q=0.9")
	req.Header.Set("Cookie", config.DouCookie)
	req.Header.Set("Priority", "u=0, i")
	req.Header.Set("Referer", "https://www.douban.com/group/")
	req.Header.Set("Sec-Ch-Ua", "\"Google Chrome\";v=\"129\", \"Not=A?Brand\";v=\"8\", \"Chromium\";v=\"129\"")
	req.Header.Set("Sec-Ch-Mobile", "?0")
	req.Header.Set("Sec-Ch-Platform", "\"Windows\"")
	req.Header.Set("Sec-Fetch-Dest", "document")
	req.Header.Set("Sec-Fetch-Mode", "navigate")
	req.Header.Set("Sec-Fetch-Site", "same-origin")
	req.Header.Set("Sec-Fetch-User", "?1")
	req.Header.Set("Upgrade-Insecure-Requests", "1")
	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/129.0.0.0 Safari/537.36")
	// 发送请求
	resp, err := client.Do(req)
	// 处理异常
	if err != nil {
		return nil, err
	}
	// 关闭流
	defer resp.Body.Close()
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return nil, err
	}
	return doc, nil
}
