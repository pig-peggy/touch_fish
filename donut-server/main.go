package main

import (
	_ "donut-server/docs" // 千万不要忘了导入生成的docs
	"donut-server/handler"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// @title 这里写标题
// @version 1.0
// @description 这里写描述信息
// @host
// @BasePath
func main() {
	r := gin.Default()
	// 本地
	r.POST("/upload", handler.GetStoryHandleFunc)
	// 网络
	r.POST("/story/chapters", handler.IndexStoryHandleFunc)
	r.POST("/story/page", handler.PageStoryHandleFunc)
	// 写作（已弃用）
	//r.POST("/writer", GetWriterHandleFunc)
	// 豆瓣
	r.POST("/dou/list", handler.GetListHandleFunc)
	r.POST("/dou/topic", handler.GetTopicHandleFunc)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":8708")
}
